<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php

require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>ruta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/crud_adviser.js"></script>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

      <div class="logo" align="center"><a href="#" class="simple-text logo-normal">
          <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li class="nav-item">
            <a class="nav-link" href="rise_massive.php">
              <i class="material-icons">content_paste</i>
              <p style="color:#FFFFFF">Asignar ruta</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="rutas_hoy.php">
              <i class="material-icons">library_books</i>
              <p>Ruta de Hoy</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sin_gestion.php">
              <i class="material-icons">bookmark_border</i>
              <p>Sin Gestion</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="reportes_encuesta.php">
            <i class="material-icons">dashboard</i>
              <p>Encuesta</p>
            </a>
          </li>


          <li class="nav-item active-pro ">
            <a class="nav-link" href="reportes.php">
              <i class="material-icons">dashboard</i>
              <p>Reportes</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link collapsed" data-toggle="collapse" href="#formsExamples" aria-expanded="false">
              <i class="material-icons">engineering</i>
              <p> Admin
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="formsExamples" style="">
              <ul class="nav">
                <li class="nav-item active">
                  <a class="nav-link" href="crear_adviser.php">
                    <i class="material-icons">person_add_alt_1</i>
                    <span class="sidebar-normal"> Agregar Asesor </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="editar_adviser.php">
                    <i class="material-icons">person_search</i>
                    <span class="sidebar-normal"> Modificar Asesor </span>
                  </a>
                </li>

              </ul>
            </div>
          </li>

        </ul>
      </div>
    </div>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">

          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>



      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">Crear Asesor</h4>
              </div>
              <div class="card-body">

                <!--------------------------------------- FORM --------------------------------------->
                <form name="<?php echo sha1('form'); ?>" id="<?php echo sha1('form'); ?>" method="post" enctype="multipart/form-data">

                  <div class="form-row">
                    <div class="col-md-4">
                      <label for="inputEmail4">Nombres <span class="text-danger">*</span></label>
                      <input type="name" value="" class="form-control" id="inputEmail4" placeholder="Ejemplo: Laura Daniela" name="names" id="name" required><br>
                    </div>

                    <div class="col-md-4">
                      <label for="inputPassword4">Apellido <span class="text-danger">*</span></label>
                      <input type="surname" value="" class="form-control" id="inputPassword4" placeholder="Ejemplo: Gonzales Rodriguez" name="surnames" id="surnames" required>
                    </div>

                    <div class="col-md-4">
                      <label for="inputAddress">Correo <span class="text-danger">*</span></label>
                      <input type="mail" value="" class="form-control" id="inputAddress" placeholder="ejemplo@ejemplo.com" name="correo" id="correo" required>
                    </div>
                    <div class="col-md-4">
                      <label for="inputAddress2">Documento <span class="text-danger">*</span></label>
                      <input type="number" value="" class="form-control" id="inputAddress2" placeholder="Numero de Documento" name="documento" id="documento" required>
                    </div>


                    <div class="col-md-4">
                      <label for="inputCity">Usuario <span class="text-danger">*</span></label>
                      <input type="text" value="" class="form-control" id="inputCity" placeholder="Nombre de Usuario Nuevo" name="name_user" id="name_user" required>
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Contrase&ntilde;a <span class="text-danger">*</span></label>
                      <input type="text" value="" class="form-control" id="inputCity" placeholder="12345678" name="password" id="password" required>
                    </div>

                    <div class="col-md-4"></div><br>
                  </div>

                  <center>
                    <div class="col-md-4"><button formaction="../../../FUNCTIONS/CRUD/crud_crear.php?<?php echo md5('fit'); ?>=<?php echo md5(mt_rand()); ?>" type="submit" class="btn btn-success mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Enviar Informaci&oacute;n </button> </div>
                  </center>
                  <div class="col-md-4"></div>


                </form>
              </div>
            </div>
          </div>
        </div>



        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="card">

              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">LISTA DE ASESORES</h4>
              </div>
              <div class="card-body">
                <div class="toolbar">
                  <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="material-datatables">
                  <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                      <div class="col-sm-12">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                          <thead>
                            <tr role="row">
                              <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" style="width:70px;" aria-label="Name: activate to sort column descending">Nombre</th>
                              <!--<th class="sorting_asc" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" style="width:70px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">COD</th>-->
                              <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" style="width: 234px;" aria-label="Position: activate to sort column ascending">Apellido</th>
                              <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" style="width: 119px;" aria-label="Office: activate to sort column ascending">Usuario</th>
                            </tr>
                          </thead>

                          <tfoot>
                            <tr>
                              <th rowspan="1" colspan="1">nombre</th>
                              <th rowspan="1" colspan="1">apellido</th>
                              <th rowspan="1" colspan="1">Usuario</th>
                            </tr>


                          </tfoot>
                          <tbody>
                            <?php $select_pdv = mysqli_query($conex, "SELECT * FROM `user` AS A LEFT JOIN userlogin AS B on A.id_user = B.id_user WHERE B.id_loginrol = '2'");
                            while ($dato = mysqli_fetch_array($select_pdv)) {

                            ?>
                              <tr height="20">
                                <td width="280"><?php echo utf8_decode($dato['names']); ?></td>
                                <td width="180"><?php echo $dato['surnames']; ?></td>
                                <td width="12"><?php echo $dato['name_user']; ?></td>
                                <td align="right" width="80" class="td-actions text-right"></td>
                              </tr>
                          </tbody>
                        <?php } ?>
                        <!--  end card  -->
                        </table>
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- end content-->


  <!-- end col-md-12 -->

  <footer class="footer">

    <div class="container-fluid">

      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>
        <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
      </div>
    </div>

  </footer>







  <!--   Core JS Files   -->

  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->


</body>

</html>