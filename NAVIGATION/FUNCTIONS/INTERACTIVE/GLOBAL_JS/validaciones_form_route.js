// JavaScript Document<script>

$(document).ready(function ubicacion() {

	$('#tipo_visita').change(function () {
		var tipo_visita = $('#tipo_visita').val();
		if (tipo_visita == "EFECTIVA") {
			$("#baner_cadena_frio").css('display', 'block');
			$("#baner_causal_no_efectiva").css('display', 'none');
			$("#baner_observacion_no_efectiva").css('display', 'none');
			$("#botonEnviarNoEfectivo").css('display', 'none');

		} else if (tipo_visita == "NO EFECTIVA") {
			$("#baner_cadena_frio").css('display', 'none');
			$("#baner_causal_no_efectiva").css('display', 'block');
			$("#baner_observacion_no_efectiva").css('display', 'block');
			$("#baner_observacion_cad_f").css('display', 'none');
			$("#baner_norditropin").css('display', 'none');
			$("#baner_saxenda").css('display', 'none');
			$("#baner_victoza").css('display', 'none');
			$("#baner_ozempic").css('display', 'none');
			$("#Compromiso").css('display', 'none');
		}
	});


	$('#causal_no_efectiva').change(function () {
		if (causal_no_efectiva != '') {
			$("#botonEnviarNoEfectivo").css('display', 'block');
		} else {

			$("#botonEnviarNoEfectivo").css('display', 'none');
		}
	});


	$('#cadena_frio').change(function () {
		var cadena_frio = $('#cadena_frio').val();
		if (cadena_frio == 'SI') {
			$("#baner_observacion_cad_f").css('display', 'none');

		} else if (cadena_frio == 'NO') {
			$("#baner_observacion_cad_f").css('display', 'block');

		}

		if (cadena_frio == 'SI' || cadena_frio == 'NO') {
			$("#baner_norditropin").css('display', 'block');
			$("#baner_saxenda").css('display', 'block');
			$("#baner_victoza").css('display', 'block');
			$("#baner_ozempic").css('display', 'block');
			$("#Compromiso").css('display', 'block');

		}
	});

	

	$('#CuentaCStock').change(function () {
		var CuentaCStock = $('#CuentaCStock').val();
		if (CuentaCStock == 'SI') {

			$("#banerPQnoStock").css('display', 'none');
			$("#banerMotAgotado").css('display', 'none');
			$("#banerAlerta").css('display', 'none');
			$("#banerPedidoTramite").css('display', 'none');

		} else if (CuentaCStock == 'NO') {
			$("#banerPQnoStock").css('display', 'block');

		}
		if (CuentaCStock == 'NO' || CuentaCStock == 'SI') {
			$("#banerRotacionSemana").css('display', 'block');

		}
	});


	$('#PorQNoStock').change(function () {
		var PorQNoStock = $('#PorQNoStock').val();
		if (PorQNoStock == 'NO LLEGA FORMULACION') {
			$("#banerMotAgotado").css('display', 'none');
			$("#banerAlerta").css('display', 'none');
			$("#banerPedidoTramite").css('display', 'none');

		} else if (PorQNoStock == 'TRANSFERENCIA') {
			$("#banerMotAgotado").css('display', 'none');
			$("#banerAlerta").css('display', 'none');
			$("#banerPedidoTramite").css('display', 'none');

		} else if (PorQNoStock == 'AGOTADO') {
			$("#banerMotAgotado").css('display', 'block');

		}
	});


	$('#MotivoAgotado').change(function () {
		var MotivoAgotado = $('#MotivoAgotado').val();
		if (MotivoAgotado == 'PEDIDO EN TRAMITE') {
			$("#banerPedidoTramite").css('display', 'block');
			$("#banerAlerta").css('display', 'none');

		} else if (MotivoAgotado == 'CAMBIA A TRANSFERENCIA') {
			$("#banerPedidoTramite").css('display', 'none');
			$("#banerAlerta").css('display', 'none');

		} else if (MotivoAgotado == 'ALERTA') {
			$("#banerPedidoTramite").css('display', 'none');
			$("#banerAlerta").css('display', 'block');

		}
	});

	$('#CantidadUnidades15').change(function () {
		var CantidadUnidades15 = $('#CantidadUnidades15').val();

		if (CantidadUnidades15 != '') {
			$("#banerCuentaDescuentos").css('display', 'block');

		} if (CantidadUnidades15 == '') {

			$("#banerCuentaDescuentos").css('display', 'none');
		}

	});

	$('#CuentaDescuentosSi').change(function () {
		var CuentaDescuentosSi = $('#CuentaDescuentosSi').val();

		if (CuentaDescuentosSi == 'SI') {
			$("#banerSiDescuento").css('display', 'block');
		}

	});
	$('#CuentaDescuentosNo').change(function () {
		var CuentaDescuentosNo = $('#CuentaDescuentosNo').val();

		if (CuentaDescuentosNo == 'NO') {
			$("#banerSiDescuento").css('display', 'none');
			$("#banerPorcentaje").css('display', 'none');
			$("#banerObservacionPorcentaje").css('display', 'none');
		}

	});
	$('#SiCuentaDescuanto').change(function () {
		var SiCuentaDescuanto = $('#SiCuentaDescuanto').val();

		if (SiCuentaDescuanto != '') {
			$("#banerPorcentaje").css('display', 'block');
			$("#banerObservacionPorcentaje").css('display', 'block');
		}

	});
	////////////////////////////////Saxenda/////////////////////////

	$('#CuentaCStockS').change(function () {
		var CuentaCStockS = $('#CuentaCStockS').val();
		if (CuentaCStockS == 'SI') {

			$("#banerPQnoStockS").css('display', 'none');
			$("#banerMotAgotadoS").css('display', 'none');
			$("#banerAlertaS").css('display', 'none');
			$("#banerPedidoTramiteS").css('display', 'none');

		} else if (CuentaCStockS == 'NO') {
			$("#banerPQnoStockS").css('display', 'block');

		}
		if (CuentaCStockS == 'NO' || CuentaCStockS == 'SI') {
			$("#banerRotacionSemanaS").css('display', 'block');

		}
	});

	$('#PorQNoStockS').change(function () {
		var PorQNoStockS = $('#PorQNoStockS').val();
		if (PorQNoStockS == 'NO LLEGA FORMULACION') {
			$("#banerMotAgotadoS").css('display', 'none');
			$("#banerAlertaS").css('display', 'none');
			$("#banerPedidoTramiteS").css('display', 'none');

		} else if (PorQNoStockS == 'TRANSFERENCIA') {
			$("#banerMotAgotadoS").css('display', 'none');
			$("#banerAlertaS").css('display', 'none');
			$("#banerPedidoTramiteS").css('display', 'none');

		} else if (PorQNoStockS == 'AGOTADO') {
			$("#banerMotAgotadoS").css('display', 'block');

		}
	});


	$('#MotivoAgotadoS').change(function () {
		var MotivoAgotadoS = $('#MotivoAgotadoS').val();
		if (MotivoAgotadoS == 'PEDIDO EN TRAMITE') {
			$("#banerPedidoTramiteS").css('display', 'block');
			$("#banerAlertaS").css('display', 'none');

		} else if (MotivoAgotadoS == 'CAMBIA A TRANSFERENCIA') {
			$("#banerPedidoTramiteS").css('display', 'none');
			$("#banerAlertaS").css('display', 'none');

		} else if (MotivoAgotadoS == 'ALERTA') {
			$("#banerPedidoTramiteS").css('display', 'none');
			$("#banerAlertaS").css('display', 'block');

		}
	});

	$('#CantidadUnidadesCa1').change(function () {
		var CantidadUnidadesCa1 = $('#CantidadUnidadesCa1').val();

		if (CantidadUnidadesCa1 != '') {
			$("#banerCuentaDescuentosS").css('display', 'block');

		} if (CantidadUnidadesCa1 == '') {

			$("#banerCuentaDescuentosS").css('display', 'none');
		}

	});

	$('#CuentaDescuentosSiS').change(function () {
		var CuentaDescuentosSiS = $('#CuentaDescuentosSiS').val();

		if (CuentaDescuentosSiS == 'SI') {
			$("#banerSiDescuentoS").css('display', 'block');
		}

	});
	$('#CuentaDescuentosNoS').change(function () {
		var CuentaDescuentosNoS = $('#CuentaDescuentosNoS').val();

		if (CuentaDescuentosNoS == 'NO') {
			$("#banerSiDescuentoS").css('display', 'none');
			$("#banerPorcentajeS").css('display', 'none');
			$("#banerObservacionPorcentajeS").css('display', 'none');
		}

	});
	$('#SiCuentaDescuantoS').change(function () {
		var SiCuentaDescuantoS = $('#SiCuentaDescuantoS').val();

		if (SiCuentaDescuantoS != '') {
			$("#banerPorcentajeS").css('display', 'block');
			$("#banerObservacionPorcentajeS").css('display', 'block');
		}

	});

	///////////////////////////////////// Victoza /////////////////////////////////////////////



	$('#CuentaCStockV').change(function () {
		var CuentaCStockV = $('#CuentaCStockV').val();

		if (CuentaCStockV == 'SI') {

			$("#banerPQnoStockV").css('display', 'none');
			$("#banerMotAgotadoV").css('display', 'none');
			$("#banerAlertaV").css('display', 'none');
			$("#banerPedidoTramiteV").css('display', 'none');

		} else if (CuentaCStockV == 'NO') {
			$("#banerPQnoStockV").css('display', 'block');

		}
		if (CuentaCStockV == 'NO' || CuentaCStockV == 'SI') {
			$("#banerRotacionSemanaV").css('display', 'block');

		}
	});

	$('#PorQNoStockV').change(function () {
		var PorQNoStockV = $('#PorQNoStockV').val();
		if (PorQNoStockV == 'NO LLEGA FORMULACION') {
			$("#banerMotAgotadoV").css('display', 'none');
			$("#banerAlertaV").css('display', 'none');
			$("#banerPedidoTramiteV").css('display', 'none');

		} else if (PorQNoStockV == 'TRANSFERENCIA') {
			$("#banerMotAgotadoV").css('display', 'none');
			$("#banerAlertaV").css('display', 'none');
			$("#banerPedidoTramiteV").css('display', 'none');

		} else if (PorQNoStockV == 'AGOTADO') {
			$("#banerMotAgotadoV").css('display', 'block');

		}
	});


	$('#MotivoAgotadoV').change(function () {
		var MotivoAgotadoV = $('#MotivoAgotadoV').val();
		if (MotivoAgotadoV == 'PEDIDO EN TRAMITE') {
			$("#banerPedidoTramiteV").css('display', 'block');
			$("#banerAlertaV").css('display', 'none');

		} else if (MotivoAgotadoV == 'CAMBIA A TRANSFERENCIA') {
			$("#banerPedidoTramiteV").css('display', 'none');
			$("#banerAlertaV").css('display', 'none');

		} else if (MotivoAgotadoV == 'ALERTA') {
			$("#banerPedidoTramiteV").css('display', 'none');
			$("#banerAlertaV").css('display', 'block');

		}
	});

	$('#CantidadUnidadesCaV2').change(function () {
		var CantidadUnidadesCaV2 = $('#CantidadUnidadesCaV2').val();

		if (CantidadUnidadesCaV2 != '') {
			$("#banerCuentaDescuentosV").css('display', 'block');

		} if (CantidadUnidadesCaV2 == '') {

			$("#banerCuentaDescuentosV").css('display', 'none');
		}

	});

	$('#CuentaDescuentosSiV').change(function () {
		var CuentaDescuentosSiV = $('#CuentaDescuentosSiV').val();

		if (CuentaDescuentosSiV == 'SI') {
			$("#banerSiDescuentoV").css('display', 'block');
		}

	});
	$('#CuentaDescuentosNoV').change(function () {
		var CuentaDescuentosNoV = $('#CuentaDescuentosNoV').val();

		if (CuentaDescuentosNoV == 'NO') {
			$("#banerSiDescuentoV").css('display', 'none');
			$("#banerPorcentajeV").css('display', 'none');
			$("#banerObservacionPorcentajeV").css('display', 'none');
		}

	});
	$('#SiCuentaDescuantoV').change(function () {
		var SiCuentaDescuantoV = $('#SiCuentaDescuantoV').val();

		if (SiCuentaDescuantoV != '') {
			$("#banerPorcentajeV").css('display', 'block');
			$("#banerObservacionPorcentajeV").css('display', 'block');
		}

	});

	////////////////////////////////////////// Ozampic ////////////////////////////////////////////


	$('#CuentaCStockO').change(function () {
		var CuentaCStockO = $('#CuentaCStockO').val();
		if (CuentaCStockO == 'SI') {

			$("#banerPQnoStockO").css('display', 'none');
			$("#banerMotAgotadoO").css('display', 'none');
			$("#banerAlertaO").css('display', 'none');
			$("#banerPedidoTramiteO").css('display', 'none');

		} else if (CuentaCStockO == 'NO') {
			$("#banerPQnoStockO").css('display', 'block');

		}
		if (CuentaCStockO == 'NO' || CuentaCStockO == 'SI') {
			$("#banerRotacionSemanaO").css('display', 'block');

		}
	});

	$('#PorQNoStockO').change(function () {
		var PorQNoStockO = $('#PorQNoStockO').val();
		if (PorQNoStockO == 'NO LLEGA FORMULACION') {
			$("#banerMotAgotadoO").css('display', 'none');
			$("#banerAlertaO").css('display', 'none');
			$("#banerPedidoTramiteO").css('display', 'none');

		} else if (PorQNoStockO == 'TRANSFERENCIA') {
			$("#banerMotAgotadoO").css('display', 'none');
			$("#banerAlertaO").css('display', 'none');
			$("#banerPedidoTramiteO").css('display', 'none');

		} else if (PorQNoStockO == 'AGOTADO') {
			$("#banerMotAgotadoO").css('display', 'block');

		}
	});


	$('#MotivoAgotadoO').change(function () {
		var MotivoAgotadoO = $('#MotivoAgotadoO').val();
		if (MotivoAgotadoO == 'PEDIDO EN TRAMITE') {
			$("#banerPedidoTramiteO").css('display', 'block');
			$("#banerAlertaO").css('display', 'none');

		} else if (MotivoAgotadoO == 'CAMBIA A TRANSFERENCIA') {
			$("#banerPedidoTramiteO").css('display', 'none');
			$("#banerAlertaO").css('display', 'none');

		} else if (MotivoAgotadoO == 'ALERTA') {
			$("#banerPedidoTramiteO").css('display', 'none');
			$("#banerAlertaO").css('display', 'block');

		}
	});

	$('#CantidadUnidadesCaO1').change(function () {
		var CantidadUnidadesCaO1 = $('#CantidadUnidadesCaO1').val();

		if (CantidadUnidadesCaO1 != '') {
			$("#banerCuentaDescuentosO").css('display', 'block');
			//$("#botonEnviarEfectivo").css('display', 'block');

		} if (CantidadUnidadesCaO1 == '') {

			$("#banerCuentaDescuentosO").css('display', 'none');
			$("#botonEnviarEfectivo").css('display', 'none');
		}

	});

	$('#CuentaDescuentosSiO').change(function () {
		var CuentaDescuentosSiO = $('#CuentaDescuentosSiO').val();

		if (CuentaDescuentosSiO == 'SI') {
			$("#banerSiDescuentoO").css('display', 'block');
		}

	});
	$('#CuentaDescuentosNoO').change(function () {
		var CuentaDescuentosNoO = $('#CuentaDescuentosNoO').val();

		if (CuentaDescuentosNoO == 'NO') {
			$("#banerSiDescuentoO").css('display', 'none');
			$("#banerPorcentajeO").css('display', 'none');
			$("#banerObservacionPorcentajeO").css('display', 'none');
		}

	});
	$('#SiCuentaDescuantoO').change(function () {
		var SiCuentaDescuantoO = $('#SiCuentaDescuantoO').val();

		if (SiCuentaDescuantoO != '') {
			$("#banerPorcentajeO").css('display', 'block');
			$("#banerObservacionPorcentajeO").css('display', 'block');
		}

	});



});
