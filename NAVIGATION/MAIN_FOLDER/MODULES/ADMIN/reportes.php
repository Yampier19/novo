<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php date_default_timezone_set("America/Bogota"); ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>ruta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
  <script>
    $(document).ready(function ubicacion() {

      $('#reporte_victoza').change(function() {

        var reporte_victoza = $('#reporte_victoza').val();
        if (reporte_victoza == "ENTRE FECHAS") {
          $("#fecha_victoza").css('display', 'block');
          $('#primerafechaV').attr('required', 'required');
          $('#segundafechaV').attr('required', 'required');
        } else {
          $("#fecha_victoza").css('display', 'none');
          $('#primerafechaV').removeAttr('required', 'required');
          $('#segundafechaV').removeAttr('required', 'required');
        }

      });

      $('#reporte_saxenda').change(function() {

        var reporte_saxenda = $('#reporte_saxenda').val();
        if (reporte_saxenda == "ENTRE FECHAS") {
          $("#fecha_saxenda").css('display', 'block');
          $('#primerafechaS').attr('required', 'required');
          $('#segundafechaS').attr('required', 'required');
        } else {
          $("#fecha_saxenda").css('display', 'none');
          $('#primerafechaS').removeAttr('required', 'required');
          $('#segundafechaS').removeAttr('required', 'required');
        }

      });

      $('#reporte_norditropin').change(function() {

        var reporte_norditropin = $('#reporte_norditropin').val();
        if (reporte_norditropin == "ENTRE FECHAS") {
          $("#fecha_norditropin").css('display', 'block');
          $('#primerafechaN').attr('required', 'required');
          $('#segundafechaN').attr('required', 'required');
        } else {
          $("#fecha_norditropin").css('display', 'none');
          $('#primerafechaN').removeAttr('required', 'required');
          $('#segundafechaN').removeAttr('required', 'required');
        }

      });

      $('#reporte_ozempic').change(function() {

        var reporte_ozempic = $('#reporte_ozempic').val();
        if (reporte_ozempic == "ENTRE FECHAS") {
          $("#fecha_ozempic").css('display', 'block');
          $('#primerafechaO').attr('required', 'required');
          $('#segundafechaO').attr('required', 'required');
        } else {
          $("#fecha_ozempic").css('display', 'none');
          $('#primerafechaO').removeAttr('required', 'required');
          $('#segundafechaO').removeAttr('required', 'required');
        }

      });

      $('#reporte_ruta').change(function() {

        var reporte_ruta = $('#reporte_ruta').val();
        if (reporte_ruta == "ENTRE FECHAS") {
          $("#fecha_ruta").css('display', 'block');
          $('#primerafechaR').attr('required', 'required');
          $('#segundafechaR').attr('required', 'required');
        } else {
          $("#fecha_ruta").css('display', 'none');
          $('#primerafechaR').removeAttr('required', 'required');
          $('#segundafechaR').removeAttr('required', 'required');
        }

      });

    });
  </script>

  <style type="text/css">
    .alert-info {
      color: #0c5460;
      background-color: #d1ecf1;
      border-color: #bee5eb;
    }

    .btn {
      color: black;
    }

    .card-header {
      background-color: #ffc107;
    }

    .btn-light:hover {
      color: black;
      background-color: #bee5eb;
      border-color: #bee5eb;
    }

    #agrupacion_n1 {
      border: 1px solid #ccc;
      margin-top: 1%;
      margin-bottom: 1%;
      padding: 1%;
    }

    #agrupacion_n2 {
      border: 1px solid #ccc;
      margin-top: 1%;
      margin-bottom: 1%;
      padding: 1%;
    }

    #subir_inventario {
      margin-top: 0.4%;
    }

    #nombre_archivo {
      padding: 7.5px;
      width: 100%;
    }

    #nombre_archivo:hover {
      background-color: #bee5eb;
    }

    .main-panel>.content {
      margin-top: 10px;
      padding: 30px 15px;
      min-height: calc(100vh - 123px);
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

      <div class="logo" align="center"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li class="nav-item">
            <a class="nav-link" href="rise_massive.php">
              <i class="material-icons">content_paste</i>
              <p style="color:#FFFFFF">Asignar ruta</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rutas_hoy.php">
              <i class="material-icons">library_books</i>
              <p>Ruta de Hoy</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sin_gestion.php">
              <i class="material-icons">bookmark_border</i>
              <p>Sin Gestion</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="reportes_encuesta.php">
            <i class="material-icons">dashboard</i>
              <p>Encuesta</p>
            </a>
          </li>


          <li class="nav-item active ">
            <a class="nav-link" href="reportes.php">
              <i class="material-icons">dashboard</i>
              <p>Reportes</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link collapsed" data-toggle="collapse" href="#formsExamples" aria-expanded="false">
              <i class="material-icons">engineering</i>
              <p> Admin
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="formsExamples" style="">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="editar_adviser.php">
                    <i class="material-icons">person_search</i>
                    <span class="sidebar-normal"> Modificar Asesor </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="crear_adviser.php">
                    <i class="material-icons">person_add_alt_1</i>
                    <span class="sidebar-normal"> Agregar Asesor </span>
                  </a>
                </li>

              </ul>
            </div>
          </li>

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
            <a style="color:#333333;" class="navbar-brand" href="javascript:;">Reportes </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
              <div class="page-categories">
                <h3 class="title text-center">Exportables en Excel <img src="../../../DESIGN/IMG/microsoft-exce.png"></h3>
                <br>
                <ul class="nav nav-pills nav-pills-info nav-pills-icons justify-content-center" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#link7" role="tablist">
                      <i class="material-icons">create</i> Victoza
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#link8" role="tablist">
                      <i class="material-icons">biotech</i> Saxenda
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#link9" role="tablist">
                      <i class="material-icons">colorize</i> Norditropin
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#link10" role="tablist">
                      <i class="material-icons">wysiwyg</i> Ozempic
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#link11" role="tablist">
                      <i class="material-icons">view_list</i> Ruta
                    </a>
                  </li>
                </ul>
                <div class="tab-content tab-space tab-subcategories">
                  <div class="tab-pane active show" id="link7">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title"><img width="32px" height="32px" src="../../../DESIGN/IMG/microsoft-exce.png"> Reporte Victoza</h4>
                        <p class="card-category">
                          Seleccione una de las opciones luego de click a Exportar
                        </p>
                      </div>
                      <form action="../REPORTS/reports?<?php echo MD5('tipo') . "=" . base64_encode('victoza');  ?>" method="post" id="form_victoza">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">

                                <select class="form-control selectpicker" data-style="btn btn-link" id="reporte_victoza" name="reporte_victoza" required>
                                  <option value="" selected="" disabled="">Seleccione...</option>
                                  <option value="FECHA HOY">FECHA HOY</option>
                                  <option value="ENTRE FECHAS">ENTRE FECHAS</option>
                                  <option value="TODO">TODO</option>
                                </select>
                              </div>


                            </div>

                            <div class="col-md-8">
                              <div style="display:none" id="fecha_victoza">
                                <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="primerafechaV" name="primerafechaV" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <center> <br><label>hasta</label></center>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="segundafechaV" name="segundafechaV" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>

                          <center>
                            <button class="btn btn-success">
                              <span class="btn-label">
                                <i class="material-icons">get_app</i>
                              </span>
                              Exportar
                              <div class="ripple-container"></div></button>
                          </center>

                        </div>

                      </form>
                    </div>
                  </div>
                  <div class="tab-pane" id="link8">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title"><img width="32px" height="32px" src="../../../DESIGN/IMG/microsoft-exce.png"> Reporte Saxenda</h4>
                        <p class="card-category">
                          Seleccione una de las opciones luego de click a Exportar
                        </p>
                      </div>
                      <form action="../REPORTS/reports?<?php echo MD5('tipo') . "=" . base64_encode('saxenda');  ?>" method="post" id="form_saxenda">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">

                                <select class="form-control selectpicker" data-style="btn btn-link" id="reporte_saxenda" name="reporte_saxenda" required>
                                  <option value="" selected="" disabled="">Seleccione...</option>
                                  <option value="FECHA HOY">FECHA HOY</option>
                                  <option value="ENTRE FECHAS">ENTRE FECHAS</option>
                                  <option value="TODO">TODO</option>
                                </select>
                              </div>


                            </div>

                            <div class="col-md-8">
                              <div style="display:none" id="fecha_saxenda">
                                <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="primerafechaS" name="primerafechaS" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <center> <br><label>hasta</label></center>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="segundafechaS" name="segundafechaS" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>

                          <center>
                            <button class="btn btn-success">
                              <span class="btn-label">
                                <i class="material-icons">get_app</i>
                              </span>
                              Exportar
                              <div class="ripple-container"></div></button>
                          </center>

                        </div>

                      </form>
                    </div>
                  </div>
                  <div class="tab-pane" id="link9">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title"><img width="32px" height="32px" src="../../../DESIGN/IMG/microsoft-exce.png"> Reporte Norditropin</h4>
                        <p class="card-category">
                          Seleccione una de las opciones luego de click a Exportar
                        </p>
                      </div>
                      <form action="../REPORTS/reports?<?php echo MD5('tipo') . "=" . base64_encode('norditropin'); ?>" method="post" id="form_norditropin">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">

                                <select class="form-control selectpicker" data-style="btn btn-link" id="reporte_norditropin" name="reporte_norditropin" required>
                                  <option value="" selected="" disabled="">Seleccione...</option>
                                  <option value="FECHA HOY">FECHA HOY</option>
                                  <option value="ENTRE FECHAS">ENTRE FECHAS</option>
                                  <option value="TODO">TODO</option>
                                </select>
                              </div>


                            </div>

                            <div class="col-md-8">
                              <div style="display:none" id="fecha_norditropin">
                                <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="primerafechaN" name="primerafechaN" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <center> <br><label>hasta</label></center>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="segundafechaN" name="segundafechaN" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>

                          <center>
                            <button class="btn btn-success">
                              <span class="btn-label">
                                <i class="material-icons">get_app</i>
                              </span>
                              Exportar
                              <div class="ripple-container"></div></button>
                          </center>

                        </div>

                      </form>
                    </div>
                  </div>
                  <div class="tab-pane" id="link10">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title"><img width="32px" height="32px" src="../../../DESIGN/IMG/microsoft-exce.png"> Reporte Ozempic</h4>
                        <p class="card-category">
                          Seleccione una de las opciones luego de click a Exportar
                        </p>
                      </div>
                      <form action="../REPORTS/reports?<?php echo MD5('tipo') . "=" . base64_encode('ozempic');  ?>" method="post" id="form_ozempic">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">

                                <select class="form-control selectpicker" data-style="btn btn-link" id="reporte_ozempic" name="reporte_ozempic" required>
                                  <option value="" selected="" disabled="">Seleccione...</option>
                                  <option value="FECHA HOY">FECHA HOY</option>
                                  <option value="ENTRE FECHAS">ENTRE FECHAS</option>
                                  <option value="TODO">TODO</option>
                                </select>
                              </div>


                            </div>

                            <div class="col-md-8">
                              <div style="display:none" id="fecha_ozempic">
                                <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="primerafechaO" name="primerafechaO" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <center> <br><label>hasta</label></center>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="segundafechaO" name="segundafechaO" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>

                          <center>
                            <button class="btn btn-success">
                              <span class="btn-label">
                                <i class="material-icons">get_app</i>
                              </span>
                              Exportar
                              <div class="ripple-container"></div></button>
                          </center>

                        </div>

                      </form>
                    </div>
                  </div>

                  <div class="tab-pane" id="link11">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title"><img width="32px" height="32px" src="../../../DESIGN/IMG/microsoft-exce.png"> Reporte Ruta</h4>
                        <p class="card-category">
                          Seleccione una de las opciones luego de click a Exportar
                        </p>
                      </div>
                      <form action="../REPORTS/reports_ruta?<?php echo MD5('tipo') . "=" . base64_encode('ruta');  ?>" method="post" id="form_ruta">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">

                                <select class="form-control selectpicker" data-style="btn btn-link" id="reporte_ruta" name="reporte_ruta" required>
                                  <option value="" selected="" disabled="">Seleccione...</option>
                                  <option value="FECHA HOY">FECHA HOY</option>
                                  <option value="ENTRE FECHAS">ENTRE FECHAS</option>
                                  <option value="TODO">TODO</option>
                                </select>
                              </div>


                            </div>

                            <div class="col-md-8">
                              <div style="display:none" id="fecha_ruta">
                                <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="primerafechaR" name="primerafechaR" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <center> <br><label>hasta</label></center>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label class="label-control bmd-label-static">&nbsp;</label>
                                      <input type="date" id="segundafechaR" name="segundafechaR" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>

                          <center>
                            <button class="btn btn-success">
                              <span class="btn-label">
                                <i class="material-icons">get_app</i>
                              </span>
                              Exportar
                              <div class="ripple-container"></div></button>
                          </center>

                        </div>

                      </form>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

        <footer class="footer">
          <div class="container-fluid">

            <div class="copyright float-right">
              &copy;
              <script>
                document.write(new Date().getFullYear())
              </script>
              <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <script>
      function setFormValidation(id) {
        $(id).validate({
          highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
            $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
          },
          success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
            $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
          },
          errorPlacement: function(error, element) {
            $(element).closest('.form-group').append(error);
          },
        });
      }

      $(document).ready(function() {
        setFormValidation('#RegisterValidation');
        setFormValidation('#TypeValidation');
        setFormValidation('#LoginValidation');
        setFormValidation('#RangeValidation');
      });
    </script>
    <style>
      #ofBar {
        background: #de2e2e;
        text-align: left;
        z-index: 999999999;
        font-size: 16px;
        color: #fff;
        padding: 18px 5%;
        font-weight: 400;
        display: block;
        position: relative;
        top: 0px;
        box-shadow: 0 6px 13px -4px rgba(0, 0, 0, 0.25);
      }

      #ofBar b {
        font-size: 15px !important;
      }

      #count-down {
        display: initial;
        padding-left: 10px;
        font-weight: bold;
      }

      #close-bar {
        font-size: 22px;
        color: #3e3947;
        margin-right: 0;
        position: absolute;
        right: 5%;
        background: white;
        opacity: 0.5;
        padding: 0px;
        height: 25px;
        line-height: 21px;
        width: 25px;
        border-radius: 50%;
        text-align: center;
        top: 18px;
        cursor: pointer;
        z-index: 9999999999;
        font-weight: 200;
      }

      #close-bar:hover {
        opacity: 1;
      }

      #btn-bar {
        background-color: #fff;
        color: #40312d;
        border-radius: 4px;
        padding: 10px 20px;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 12px;
        opacity: .95;
        margin-left: 15px;
        top: 0px;
        position: relative;
        cursor: pointer;
        text-align: center;
        box-shadow: 0 5px 10px -3px rgba(0, 0, 0, .23), 0 6px 10px -5px rgba(0, 0, 0, .25);
      }

      #btn-bar:hover {
        opacity: 0.9;
      }

      #btn-bar {
        opacity: 1;
      }

      #btn-bar span {
        color: red;
      }

      .right-side {
        float: right;
        margin-right: 60px;
        top: -6px;
        position: relative;
        display: block;
      }

      #oldPriceBar {
        text-decoration: line-through;
        font-size: 16px;
        color: #fff;
        font-weight: 400;
        top: 2px;
        position: relative;
      }

      #newPrice {
        color: #fff;
        font-size: 19px;
        font-weight: 700;
        top: 2px;
        position: relative;
        margin-left: 7px;
      }

      #fromText {
        font-size: 15px;
        color: #fff;
        font-weight: 400;
        margin-right: 3px;
        top: 0px;
        position: relative;
      }

      @media(max-width: 991px) {
        .right-side {
          float: none;
          margin-right: 0px;
          margin-top: 5px;
          top: 0px
        }

        #ofBar {
          padding: 50px 20px 20px;
          text-align: center;
        }

        #btn-bar {
          display: block;
          margin-top: 10px;
          margin-left: 0;
        }
      }

      @media (max-width: 768px) {
        #count-down {
          display: block;
          font-size: 25px;
        }
      }
    </style>
    <!--   Core JS Files   -->

    <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
    <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Plugin for the momentJs  -->
    <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Chartist JS -->
    <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->

    <script>
      $(document).ready(function() {
        $('#datatables').DataTable({
          "pagingType": "full_numbers",
          "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "Tot"]
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Buscar registros",
          }
        });

        var table = $('#datatable').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
          $tr = $(this).closest('tr');
          var data = table.row($tr).data();
          alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
          $tr = $(this).closest('tr');
          table.row($tr).remove().draw();
          e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
          alert('You clicked on Like button');
        });
      });
    </script>
    <script>
      $(document).ready(function() {
        $().ready(function() {
          $sidebar = $('.sidebar');

          $sidebar_img_container = $sidebar.find('.sidebar-background');

          $full_page = $('.full-page');

          $sidebar_responsive = $('body > .navbar-collapse');

          window_width = $(window).width();

          fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

          if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
              $('.fixed-plugin .dropdown').addClass('open');
            }

          }

          $('.fixed-plugin a').click(function(event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
              if (event.stopPropagation) {
                event.stopPropagation();
              } else if (window.event) {
                window.event.cancelBubble = true;
              }
            }
          });

          $('.fixed-plugin .active-color span').click(function() {
            $full_page_background = $('.full-page-background');

            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('color');

            if ($sidebar.length != 0) {
              $sidebar.attr('data-color', new_color);
            }

            if ($full_page.length != 0) {
              $full_page.attr('filter-color', new_color);
            }

            if ($sidebar_responsive.length != 0) {
              $sidebar_responsive.attr('data-color', new_color);
            }
          });

          $('.fixed-plugin .background-color .badge').click(function() {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('background-color');

            if ($sidebar.length != 0) {
              $sidebar.attr('data-background-color', new_color);
            }
          });

          $('.fixed-plugin .img-holder').click(function() {
            $full_page_background = $('.full-page-background');

            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');


            var new_image = $(this).find("img").attr('src');

            if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
              $sidebar_img_container.fadeOut('fast', function() {
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $sidebar_img_container.fadeIn('fast');
              });
            }

            if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
              var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

              $full_page_background.fadeOut('fast', function() {
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                $full_page_background.fadeIn('fast');
              });
            }

            if ($('.switch-sidebar-image input:checked').length == 0) {
              var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
              var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            }

            if ($sidebar_responsive.length != 0) {
              $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
          });

          $('.switch-sidebar-image input').change(function() {
            $full_page_background = $('.full-page-background');

            $input = $(this);

            if ($input.is(':checked')) {
              if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeIn('fast');
                $sidebar.attr('data-image', '#');
              }

              if ($full_page_background.length != 0) {
                $full_page_background.fadeIn('fast');
                $full_page.attr('data-image', '#');
              }

              background_image = true;
            } else {
              if ($sidebar_img_container.length != 0) {
                $sidebar.removeAttr('data-image');
                $sidebar_img_container.fadeOut('fast');
              }

              if ($full_page_background.length != 0) {
                $full_page.removeAttr('data-image', '#');
                $full_page_background.fadeOut('fast');
              }

              background_image = false;
            }
          });

          $('.switch-sidebar-mini input').change(function() {
            $body = $('body');

            $input = $(this);

            if (md.misc.sidebar_mini_active == true) {
              $('body').removeClass('sidebar-mini');
              md.misc.sidebar_mini_active = false;

              $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

            } else {

              $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

              setTimeout(function() {
                $('body').addClass('sidebar-mini');

                md.misc.sidebar_mini_active = true;
              }, 300);
            }

            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function() {
              window.dispatchEvent(new Event('resize'));
            }, 180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function() {
              clearInterval(simulateWindowResize);
            }, 1000);

          });
        });
      });
    </script>
</body>

</html>