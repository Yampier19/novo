<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php

require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Encuesta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/crud_adviser.js"></script>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>

  <script>
    $(document).ready(function() {
      $("#enviarEncuesta").click(function() {
        var cadena = $("#cadena").val();

        if (cadena == '') {
          swal({
            icon: 'warning',
            title: 'CADENA no puede estar vacio',
            text: '',
            timer: 2000
          })
        }
      })
    })
  </script>
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

      <div class="logo" align="center"><a href="#" class="simple-text logo-normal">
          <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li class="nav-item">
            <a class="nav-link" href="table_route.php">
              <i class="material-icons">content_paste</i>
              <p style="color:#FFFFFF">Ruta de Hoy</p>
            </a>
          </li>
          <li class="nav-item active ">
            <a class="nav-link" href="table_route.php">
              <i class="material-icons">how_to_reg</i>
              <p style="color:#FFFFFF">Encuesta</p>
            </a>
          </li>
          <li class="nav-item">
                        <a class="nav-link" href="table_encuesta.php">
                            <i class="material-icons">table_chart</i>
                            <p style="color:#FFFFFF">Encuesta Datos</p>
                        </a>
                    </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
            <a style="color:#333333;" class="navbar-brand" href="javascript:;">
              <script>
                var f = new Date();
                document.write(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear());
              </script>
            </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>



      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">Formulario</h4><br>
              </div>
              <div class="card-body">

                <!--------------------------------------- FORM --------------------------------------->
                <form name="<?php echo sha1('form'); ?>" id="<?php echo sha1('form'); ?>" method="post" enctype="multipart/form-data">

                  <div class="form-row">
                    <!--Inicio del Form -->

                    <!-- INFORMACION DEL PUNTO  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                          Informaci&oacute;n del Punto:
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="inputEmail4">Cadena <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="cadena" id="cadena" required><br>
                    </div>

                    <div class="col-md-4">
                      <label for="inputPassword4">Nombre PDV <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="pdv">
                    </div>

                    <div class="col-md-4">
                      <label for="inputAddress">Codigo <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="codigo">
                    </div>
                    <div class="col-md-4">
                      <label for="inputAddress2">Canal <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="canal">
                    </div>


                    <div class="col-md-4">
                      <label for="inputCity">Nombre Funcionario <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="nombre">
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Cargo <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="cargo">
                    </div>
                    <!-- FIN DE INFORMACION DEL PUNTO  -->


                    <div class="col-md-12">
                      <p> </p>
                    </div>


                    <!-- INICIO DE PREGUNTAS  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                          Preguntas:
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-3">
                      <label for="curso">1. Ha realizado capacitaciones, cursos, entrenamientos de farma por parte de algun laboratorio? <span class="text-danger"> *</span></label>
                      Si <input type="radio" value="SI" class="selectpicker" name="cursos" id="cursosSi">
                      No <input type="radio" value="NO" class="selectpicker" name="cursos" id="cursosNo">
                    </div>

                    <div class="col-md-2 target" style="display: none;">
                      <div>
                        <p> </p>
                      </div>
                      <label for="cursos2">Recibi&oacute; certificaci&oacute;n?</label><br>
                      Si <input type="radio" value="SI" class="selectpicker" name="cursoSi" id="cursoSi">
                      No <input type="radio" value="NO" class="selectpicker" name="cursoSi" id="cursoSi">
                    </div>


                    <div class="col-md-4">
                      <label for="formacion">2. Estaria interesado en realizar capacitaciones, cursos o entrenamientos virtuales certificados? <span class="text-danger">*</span></label>
                      Si <input type="radio" value="SI" class="selectpicker" name="formacion" id="formacionSi">
                      No <input type="radio" value="NO" class="selectpicker" name="formacion" id="formacionNo">
                    </div>

                    <div class="col-md-3 targetDos" style="display: none;">
                      <div>
                        <p> </p>
                      </div>
                      <label for="formaciones">Cu&aacute;l cree que la Instituci&oacute;n indicada para certificarlo?</label>
                      <input class="form-control" type="text" name="formaciones" id="formaciones"><br>
                    </div>

                    <div class="col-md-4">
                      <label for="disponibilidad">3. Con que disponibilidad cuenta para dichos entrenamientos<span class="text-danger">*</span></label>
                      <select class="form-control selectpicker" data-style="btn btn-link" name="disponibilidad" id="disponibilidad">
                        <option value="">Seleccione...</option>
                        <option value="semanaAm">Entre Semana - AM</option>
                        <option value="semanaPm">Entre Semana - PM</option>
                        <option value="fdsAm">Fines de Semana - AM</option>
                        <option value="fdsPm">Fines de Semana - PM</option>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <label for="modalidad">4. Que modalidad prefiere teniendo en cuenta la nueva Normalidad <span class="text-danger">*</span></label>
                      <select class="form-control selectpicker" data-style="btn btn-link" name="modalidad" id="modalidad">
                        <option value="">Seleccione...</option>
                        <option value="virtual">Virtual</option>
                        <option value="presencial">Presencial</option>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <label for="modalidad">5. En que temas le gustaria que se le capacitara <span class="text-danger">*</span></label>
                      <p> </p>
                      <input class="form-control" type="text" name="temaCapacitacion" id="temaCapacitacion">
                    </div>
                    <!-- FIN PREGUNTAS -->

                  </div><br> <!-- Fin Form -->

                  <center>
                    <div class="col-md-4"><button id="enviarEncuesta" formaction="../../../FUNCTIONS/CRUD/encuestaInsert.php?<?php echo md5('fit'); ?>=<?php echo md5(mt_rand()); ?>" type="submit" class="btn btn-success mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Enviar Informaci&oacute;n </button> </div>
                  </center>
                  <div class="col-md-4"></div>


                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- end content-->


  <!-- end col-md-12 -->

  <footer class="footer">

    <div class="container-fluid">

      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>
        <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
      </div>
    </div>

  </footer>

  <!--   Core JS Files   -->

  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->

  <!-- Script -->
  <script>
    $(document).ready(function() {
      //  Primera Pregunta
      $("#cursosSi").on("click", function() {
        $('.target').show(); //muestro mediante clase
      });
      $("#cursosNo").on("click", function() {
        $('.target').hide(); //muestro mediante clase
      });

      //  Segunda Pregunta
      $("#formacionSi").on("click", function() {
        $('.targetDos').show(); //muestro mediante clase
      });
      $("#formacionNo").on("click", function() {
        $('.targetDos').hide(); //muestro mediante clase
      });
    })
  </script>
  <!-- Fin de Scripts -->

</body>

</html>