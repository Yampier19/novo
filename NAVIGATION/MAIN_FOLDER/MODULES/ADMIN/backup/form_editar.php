<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php 
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

 ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>ruta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../../../DESIGN/CSS/estile_adicional.css"/>
  <!-- CSS Just for demo purpose, don't include it in your project -->
 <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
 <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/separador_miles.js"></script>
 <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/validaciones_form_route.js"></script>	
<script type="text/javascript">
/*function iniciar(){
var boton=document.getElementById('obtener');
boton.addEventListener('click', obtener, false);
}*/
function obtener(){
var parametros={enableHighAccuracy:true}
	navigator.geolocation.getCurrentPosition(mostrar, gestionarErrores,parametros);
}
function mostrar(posicion){
//var ubicacion=document.getElementById('localizacion');
//var datos='';
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
/*datos+='Latitud: '+posicion.coords.latitude+'<br>';
datos+='Longitud: '+posicion.coords.longitude+'<br>';
datos+='Exactitud: '+posicion.coords.accuracy+' metros.<br>';
ubicacion.innerHTML=datos;*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("LONG").value=longitud;
/*swal(
			{title: 'Bienvenido estamos usando GPS',
		confirmButtonColor: '#17a2b8'});*/
}
 
 function cerrar(){
 	 	  	<?php
  //sleep(10);
  	?>
	window.onload = window.top.location.href = "../../CONNECTION/SECURITY/cerrar_sesion.php";
 }
function gestionarErrores(error){
	if(error.code==1)
	{
	swal(
		{title: 'Debes permitir el uso de la geolocalizacion en tu navegador',
  	confirmButtonColor: '#17a2b8'}).then(function () {
     window.top.location.href = "../../CONNECTION/SECURITY/cerrar_sesion.php";
    });
	}
}

<?php
//if (isset($_GET['x'])) {
?>
window.addEventListener('load', obtener, false);
<?php
//}
?>
</script>

 <script type="text/javascript">
 $(document).ready(function separadordemiles(){
 function dir()
{
 var titlePDV = $('titlePDV').val();
 var farmacia_pdv = $('#farmacia_pdv').val();

$('#titlePDV').val(farmacia_pdv);

}

$('#farmacia_pdv').change(function()
	{
	  dir();
	});
 
 });
 
    function ajustar() {
        var texto=document.getElementById("titlePDV");
        var txt=texto.value;
        var tamano=txt.length;
        tamano*=10; //el valor multiplicativo debe cambiarse dependiendo del tama?o de la fuente
        texto.style.width=tamano+"px";
    }
    </script>
	
 <!--- validation--------->
 
   
</head>

<body class="">
   <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
	
      <div class="logo" align="center"><a href="#" class="simple-text logo-normal">
          <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          
          <li class="nav-item">
            <a class="nav-link" href="rise_massive.php">
              <i class="material-icons">content_paste</i>
              <p style="color:#FFFFFF">Asignar ruta</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="rutas_hoy.php">
              <i class="material-icons">library_books</i>
              <p >Ruta de Hoy</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="login_history.php">
              <i class="material-icons">person_pin_circle</i>
              <p>Login History</p>
            </a>
          </li>
          
         
          <li class="nav-item active-pro ">
            <a class="nav-link" href="#">
              <i class="material-icons">dashboard</i>
              <p>Reportes</p>
            </a>
          </li>
		  <li class="nav-item ">
            <a class="nav-link collapsed" data-toggle="collapse" href="#formsExamples" aria-expanded="false">
              <i class="material-icons">engineering</i>
              <p> Admin
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="formsExamples" style="">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                     <i class="material-icons">person_search</i>
                    <span class="sidebar-normal"> Modificar Asesor </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                     <i class="material-icons">person_add_alt_1</i>
                    <span class="sidebar-normal"> Agregar Asesor </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <i class="material-icons">bubble_chart</i>
                    <span class="sidebar-normal">Activacion Asesor</span>
                  </a>
                </li>
               
              </ul>
            </div>
          </li>
		  
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
         
		   
		   <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
<a style="color:#333333;" class="navbar-brand" href="javascript:;">Ruta <?php date_default_timezone_set("America/Bogota"); $select_pdv = mysqli_query($conex,"SELECT * FROM `pdv_farmacia` WHERE `id_pdv` = '".base64_decode($_GET[base64_encode('id_pdv')])."'"); 
		  while($dato = mysqli_fetch_array($select_pdv)){
		  
		  $id_pdv = $dato['id_pdv'];
		  $cod_pdv =  $dato['cod_pdv'];
		  $nombre_pdv =  $dato['nombre_pdv'];
		  $ciudad_pdv = $dato['ciudad_pdv'];
		  $direccion_pdv =  $dato['direccion_pdv'];
		  $telefono =  $dato['telefono'];
		  $cadena_pdv = $dato['cadena_pdv'];
		  $canal_pdv = $dato['canal_pdv'];
		  $nombre_regente = $dato['nombre_regente'];
		  $num_regente = $dato['num_regetente'];
		  $convenio = $dato['convenio'];
		  $categoria = $dato['categoria'];
		  $visita = $dato['visita'];
		  $tipo_gestion = $dato['tipo_gestion'];
		  
		  }
		  
		   ?> <script>
              var f = new Date();
document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
            </script></a> <?php if($cod_pdv != ''){ echo $cod_pdv; }else{ echo 'N/A'; }?>&nbsp;&nbsp;&nbsp;
           </div>
		   
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
	  
  <div class="content">
  <?php 
  $consul_ruta= mysqli_query($conex,"SELECT * FROM `pdv_farmacia` AS A LEFT JOIN pdv_efectivo AS B ON A.id_pdv = B.id_pdv LEFT JOIN norditropin AS C ON A.id_pdv = C.id_pdv LEFT JOIN saxenda AS D ON A.id_pdv = D.id_pdv LEFT JOIN victoza AS E ON A.id_pdv = E.id_pdv LEFT JOIN ozempic AS F ON A.id_pdv = F.id_pdv WHERE DATE(B.fecha_registro) = '2020-07-23' AND  A.`id_pdv` = '".base64_decode($_GET[base64_encode('id_pdv')])."'"); 
  
  $conteo_efect = mysqli_num_rows($consul_ruta);
  
  if($conteo_efect > 0){
  
  while($datoEfec=mysqli_fetch_array($consul_ruta)){
  
    
  
  }
    
  }
  
  
  
  ?>
  <form  name="<?php echo sha1('form'); ?>" id="<?php echo sha1('form'); ?>" method="post" enctype="multipart/form-data">
  <div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off" readonly value=""/>
        <input id="LONG" name="LONG" type="text" class="form-control" autocomplete="off" readonly value=""/>
    </div>
        <div class="container-fluid">
		<div class="card">
		<div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title" style="width:auto;" ><input onKeyUp="ajustar()" id="titlePDV"  class="sinborde" value="<?php echo $nombre_pdv;  ?>" readonly="" /></h4>
            </div>
          </div>
                
                <div class="card-body">
                  <div id="accordion" role="tablist">
				  <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Informacion punto------------>
                    <div class="card-collapse">
                      <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                          <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                            Informaci&oacute;n del Punto
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
					  <!------->
                        <div class="card-body">
						<div class="row">
						<div class="col-md-4">
					<div class="row">
					  <label class="col-sm-3 col-form-label"><i class="fa fa-link" aria-hidden="true"></i> Cadena</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled"><input type="hidden" name="<?php echo crc32('id_pdv'); ?>" id="<?php echo crc32('id_pdv'); ?>"  value="<?php echo base64_encode($id_pdv); ?>"/>
                          <input type="text" class="form-control" value="<?php echo $cadena_pdv; ?>" disabled="" id="cadena_pdv" name="cadena_pdv" >
                        </div>
                      </div>
					  </div>
					</div>
                      
					<div class="col-md-4">
					<div class="row">
					  <label class="col-sm-4 col-form-label"><i class="fa fa-building-o" aria-hidden="true"></i> Ciudad</label>
                      <div class="col-sm-8">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $ciudad_pdv; ?>" disabled="" id="ciudad_pdv" name="ciudad_pdv">
                        </div>
                      </div>
					  </div>
					  </div>
					
					<div class="col-md-4">
					  <div class="row">
					  <label class="col-sm-3 col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> Tel&eacute;fono</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $telefono; ?>" id="telefono" name="telefono" autocomplete="off">
                        </div>
                      </div>
					  </div>
					  <!-- 
					  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">
        <i class="fa fa-phone" aria-hidden="true"></i> Telefono
      </span>
    </div>
    <input type="text" class="form-control" value="3650097-3118127105" disabled="">
  </div>
					  --->
                    </div>
					
					
					</div>
					<!---------------- 2--->
					<div class="row">
					
					<div class="col-md-6">
					<div class="row">
					  <label class="col-sm-3 col-form-label"><i class="fa fa-home" aria-hidden="true"></i> Direcci&oacute;n</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $direccion_pdv; ?>" id="direccion_pdv" name="direccion_pdv" autocomplete="off">
                        </div>
                      </div>
					  </div>
					  </div>
                      
					  <div class="col-md-6">
					<div class="row">
					  <label class="col-sm-3 col-form-label"><i class="fa fa-medkit" aria-hidden="true"></i> Farmacia</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $nombre_pdv; ?>" id="farmacia_pdv" name="farmacia_pdv" onKeyUp="ajustar()" autocomplete="off">
                        </div>
                      </div>
					  </div>
					  </div>
					
					
					
					</div>
					<!--------------- 3---->
					<div class="row">
					<div class="col-md-4">
					<div class="row">
					  <label class="col-sm-3 col-form-label">Canal</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $canal_pdv; ?>" id="canal_pdv" name="canal_pdv" autocomplete="off">
                        </div>
                      </div>
					  </div>
					</div>
                      <div class="col-md-4">
					  <div class="row">
					  <label class="col-sm-5 col-form-label"><i class="fa fa-user-circle" aria-hidden="true"></i> Nombre Regente</label>
                      <div class="col-sm-7">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $nombre_regente;  ?>" id="nombre_regente" name="nombre_regente" autocomplete="off">
                        </div>
                      </div>
					  </div>
                    </div>
					<div class="col-md-4">
					<div class="row">
					  <label class="col-sm-5 col-form-label">Numero regentes</label>
                      <div class="col-sm-7">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="number" min='1' max='10' class="form-control" value="<?php echo $num_regente; ?>" id="numero_regente" name="numero_regente" >
                        </div>
                      </div>
					  </div></div>
					
					
					</div>
					
					<!----------4--------->
					<div class="row">
					<div class="col-md-4">
					<div class="row">
					  <label class="col-sm-3 col-form-label">Convenio</label>
                      <div class="col-sm-9">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $convenio; ?>" id="convenio" name="convenio" >
                        </div>
                      </div>
					  </div>
					</div>
                      <div class="col-md-4">
					  <div class="row">
					  <label class="col-sm-5 col-form-label">Tipo de gesti&oacute;n</label>
                      <div class="col-sm-7">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="text" class="form-control" value="<?php echo $tipo_gestion; ?>" id="tipo_gestion" name="tipo_gestion">
                        </div>
                      </div>
					  </div>
                    </div>
					<div class="col-md-4">
					<div class="row">
						<label class="col-sm-4 col-form-label">Tipo de Visita <span class="text-danger">*</span></label>
						<div class="col-sm-8">
						  <select class="form-control selectpicker" data-style="btn btn-info btn" title="Seleccione..." id="tipo_visita" name="tipo_visita">
						    <option disabled="" selected="">Seleccione...</option>
                            <option value="EFECTIVA">Efectiva</option>
                            <option value="NO EFECTIVA">No Efectiva</option>
                          
                          </select>
						  </div>
						  </div>
					
					</div>
					
					
					</div><br>
					<!---------5----------->
					<div class="row">
					
					<div class="col-md-4 col-sm-4" >
                      <label for="exampleFormControlTextarea1"><i class="fa fa-camera" aria-hidden="true"></i> Evidencia del sitio </label>
                      <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                          <img src="../../../DESIGN/IMG/tienda.jpg" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                        <div>
                          <span class="btn btn-info btn-round btn-file">
                            <span class="fileinput-new">Tomar Foto</span>
                            <span class="fileinput-exists">Cambio</span>
                            <input type="hidden" value="" name="..."><input type="file" name="evidencia_sitio" id="evidencia_sitio">
                          <div class="ripple-container"></div></span>
                          <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Eliminar<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 63.4063px; top: 15.1406px; background-color: rgb(255, 255, 255); transform: scale(15.5098);"></div></div></a>
                        </div>
                      </div>
                    </div>
					
					
					<div class="col-md-6 col-sm-6" >
					<div class="row" id="baner_cadena_frio" style="display:none;">
				<div class="col-md-6" >
				<div class="form-group">
    <label for="exampleFormControlSelect1">Cadena de Fr&iacute;o <span class="text-danger" >*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link"  id="cadena_frio" name="cadena_frio">
      <option value="" selected="" disabled="">Seleccione...</option>
      <option value="SI">SI</option>
      <option value="NO">NO</option>
      
    </select>
  </div>
				</div><br>
				</div>
					
                     <div class="form-group" id="baner_observacion_cad_f"  style="display:none;">
    <label for="exampleFormControlTextarea1">Observaci&oacute;n</label><br>
    <textarea class="form-control" id="cadenaFrioObser"  name="cadenaFrioObser" rows="3"></textarea>
  </div>
  
				<div class="row" id="baner_causal_no_efectiva"  style="display:none;">
				<div class="col-md-8" >
				<div class="form-group">
    <label for="exampleFormControlSelect1">Causal No Efectiva <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link"  id="causal_no_efectiva" name="causal_no_efectiva">
      <option>Seleccione...</option>
      <option>PDV Cerrado Temporal</option>
      <option>PDV Cerrado definitivamente</option>
	   <option>No atiende</option>
      
    </select>
  </div>
				</div><br>
				</div>
				
				 <div class="form-group" id="baner_observacion_no_efectiva" style="display:none;">
    <label for="exampleFormControlTextarea1">Observaci&oacute;n </label><br>
    <textarea class="form-control" id="ObsCausalNoEfec" name="ObsCausalNoEfec" rows="3"></textarea>
  </div>
 
  
  <div class="row">
  <?php $select_info_efectivo = mysqli_query($conex,"SELECT * FROM `pdv_efectivo` WHERE `id_pdv` = '$id_pdv' ORDER BY `pdv_efectivo`.`id_efectivo` DESC LIMIT 1"); 
  
  while($datoE = mysqli_fetch_array($select_info_efectivo)){
  
  $objetivo = $datoE['objetivo'];
  $compromiso = $datoE['compromiso'];
  
  }
  
   ?>
  
  <button type="button" class="btn btn-round" data-toggle="modal" data-target="#signupModal">
    <i class="material-icons">assignment</i>
    Mas Informaci&oacute;n
</button>

<div class="modal fade" id="signupModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-signup" role="document">
    <div class="modal-content">
      <div class="card card-signup card-plain">
        <div class="modal-header">
          <h5 class="modal-title card-title"><?php echo $nombre_pdv; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
          </button>
        </div>
        
		<!--------------------------------------------------------------------------------------------------------------------------->
		
		<div class="modal-body">
          
		  <div class="row">
<!------------------------------------------------------------------------------------------------------------------------>		  
            <div class="col-md-5 ml-auto">
              <div class="info info-horizontal">
                <div class="icon icon-rose">
                  <i class="material-icons">timeline</i>
                </div>
                <div class="description">
                  <h4 class="info-title">Compromiso</h4>
                  <p class="description">
                  <?php echo $compromiso; ?>
                  </p>
                </div>
              </div>
 </div>
<!------------------------------------------------------------------------------------------------------------------------>
            <div class="col-md-5 mr-auto">
              <div class="info info-horizontal">
                <div class="icon icon-primary">
                  <i class="material-icons">code</i>
                </div>
                <div class="description">
                  <h4 class="info-title">Objetivo</h4>
                  <p class="description" id="verObj">
                 <?php echo $objetivo; ?>
                  </p>
				  <p class="description" id="editarObj" style="display:none"><textarea class="form-control" name="objetivo" id="objetivo"><?php echo $objetivo; ?></textarea></p>
                </div>
				</div>
             </div>
<!------------------------------------------------------------------------------------------------------------------------>
			</div>
			
	<div class="row">
	<div class="form-check" >
                  <label class="form-check-label">
                      <input class="form-check-input" type="checkbox" value="" checked>
                      <span class="form-check-sign">
                          <span class="check"></span>
                      </span>
                      Seleccione si desea Modificar el <a style="color:#00bcd4">Objetivo</a>.
                  </label>
                </div>
                </div>
              <div class="modal-footer justify-content-center">
              <a  class="btn btn-info btn-round" data-dismiss="modal" aria-label="Close" style="color:#FFFFFF;">Guardar</a>
              </div>
	
	</div>		
			
        </div>
      </div>
    </div>
  </div>

		
		<!------------------------------------------------------------------------------------------------------------------------>
      </div>
    </div>
  </div>
</div>
  
  </div>
									 
                    </div>
					
				<!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Norditropin------------>
					
                    <div class="card-collapse" id="baner_norditropin" >
                      <div class="card-header" role="tab" id="headingTwo">
                        <h5 class="mb-0">
                          <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                           Norditropin <span class="text-danger">*</span>
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
						

                          <div class="row">
						  <div class="col-md-4">
						  <div class="form-group">
    <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStock" name="CuentaCStock">
      <option value=""  disabled="" selected="">Seleccione...</option>
      <option value="SI">SI</option>
      <option value="NO">NO</option>
      
    </select>
  </div>				
   </div>
						  
						  <div class="col-md-4" style="display:none;" id="banerPQnoStock">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Porque no cuenta con stock  <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStock" name="PorQNoStock">
      <option value="" selected=""  disabled="">Seleccione...</option>
      <option value="NO LLEGA FORMULACION">NO LLEGA FORMULACI&Oacute;N</option>
      <option value="TRANSFERENCIA">TRANSFERENCIA</option>
	  <option value="AGOTADO">AGOTADO</option>
	   <option value="NO VECTORIZADO">NO VECTORIZADO</option>
      
    </select>
  </div>
  </div>
						  <div class="col-md-4" style="display:none;" id="banerMotAgotado">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Motivo de agotado</label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado">
      <option  disabled="" value="" selected="">Seleccione...</option>
      <option value="PEDIDO EN TRAMITE">PEDIDO EN TR&Aacute;MITE</option>
      <option value="CAMBIA A TRANSFERENCIA">CAMBIA A TRANSFERENCIA</option>
	  <option value="ALERTA">ALERTA</option>
      
    </select>
  </div>
  </div>
  </div><br>
  <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->		
  <div class="row">
						  <div class="col-md-4"  style="display:none" id="banerPedidoTramite">
						  <div class="form-group">
    <label class="label-control">Fecha aproximada de llegada</label><br>
    <input type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegada" name="fechaApoxiLlegada" class="form-control"  value='<?php echo date('Y-m-d'); ?>'/>
</div>	
			
   </div>
   
   <div class="col-md-4" style="display:none" id="banerAlerta">
   <div class="form-group">
    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
    <textarea class="form-control" id="alerta_observacion" name="alerta_observacion" rows="3"></textarea>
  </div>
   
   </div>					  
  </div>
 <!-------------------------------------3----<<<<<<<<<<<<<<<<<<<<<-->	
 <div style="display:none" id="banerRotacionSemana">
 <div class="row" >
 <div class="col-md-4">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label>
    <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemana" name="rotacionxsemana">
      
  </div>
  </div> </div>
 
  
  	  <div class="row">
	  
	  <div class="col-md-6">
	  <label for="exampleFormControlSelect1"><b>Norditropin 5mg</b></label>
	  <div class="row">
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades 5mg</label>
    <input type="number"  class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidades5" name="CantidadUnidades5" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio 5mg</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin5p" name="Norditropin5p" >
      
  </div>
	  </div>
	  </div>
	  </div>
	  
	  <div class="col-md-6">
	    <label for="exampleFormControlSelect1"><b>Norditropin 10mg</b></label>
	  <div class="row">
	  
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades 10mg</label>
    <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidades10" name="CantidadUnidades10" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio 10gm</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin10p" name="Norditropin10p">
      
  </div>
	  
	  </div>
	  
	  </div>
	    </div>
	    </div>
		
		
	<!------------------------------////////////////////////////////////////--------------------------->
	
	<div class="row">
	  
	  <div class="col-md-6">
	  <label for="exampleFormControlSelect1"><b>Norditropin 15mg</b></label>
	  <div class="row">
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades 15mg</label>
    <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidades15" name="CantidadUnidades15" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio 15mg</label>
 <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin15p" name="Norditropin15p"  >
      
  </div>
	  </div>
	  </div>
	  </div>
	  
	  <div class="col-md-6">
	   
	  
	    </div>
	    </div>
	
	
	  </div>
	   <!-------------------------------------4----<<<<<<<<<<<<<<<<<<<<<-->	
 <div class="row">
 <div class="col-md-4" style="display:none" id="banerCuentaDescuentos">
  <label for="exampleFormControlSelect1" >Cuenta con descuentos</label>
	 <div class="form-check form-check-radio">
    <label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentos" id="CuentaDescuentosSi" value="SI" >
        SI
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
	<label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentos" id="CuentaDescuentosNo" value="NO" >
        NO
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
</div>
<div class="form-group" style="display:none;" id="banerSiDescuento">
<label for="exampleFormControlSelect1" >Descuento</label>
      <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuanto" name="SiCuentaDescuanto">
      <option value="" selected="" disabled="">Seleccione...</option>
      <option>Virtual</option>
      <option>Presencial</option>
	  <option>Mixto</option>
      
    </select>
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerPorcentaje">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Porcentaje</label>
    <input type="number" maxlength="2" max="99" min='1' class="form-control selectpicker" data-style="btn btn-link" name="porcentaje" id="porcentaje" style="width:50px">
      
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerObservacionPorcentaje">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="observacionN" id="observacionN"></textarea>
  </div>
 </div>
 <div class="col-md-4"></div>
 </div>						  
					 </div>
                      </div>
                    </div>
					
					<!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Saxenda------------>
					
                   <div class="card-collapse" id="baner_saxenda" >
                      <div class="card-header" role="tab" id="headingThree">
                        <h5 class="mb-0">
                          <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Saxenda <span class="text-danger">*</span>
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
						

                          <div class="row">
						  <div class="col-md-4">
						  <div class="form-group">
    <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockS" name="CuentaCStockS">
      <option value=""  disabled="" selected="">Seleccione...</option>
      <option value="SI">SI</option>
      <option value="NO">NO</option>
      
    </select>
  </div>				
   </div>
						  
						  <div class="col-md-4" style="display:none;" id="banerPQnoStockS">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Porque no cuenta con stock  <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStockS" name="PorQNoStockS">
      <option value="" selected=""  disabled="">Seleccione...</option>
      <option value="NO LLEGA FORMULACION">NO LLEGA FORMULACI&Oacute;N</option>
      <option value="TRANSFERENCIA">TRANSFERENCIA</option>
	  <option value="AGOTADO">AGOTADO</option>
	   <option value="NO VECTORIZADO">NO VECTORIZADO</option>
      
    </select>
  </div>
  </div>
						  <div class="col-md-4" style="display:none;" id="banerMotAgotadoS">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Motivo de agotado</label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoS">
      <option  disabled="" value="" selected="">Seleccione...</option>
      <option value="PEDIDO EN TRAMITE">PEDIDO EN TR&Aacute;MITE</option>
      <option value="CAMBIA A TRANSFERENCIA">CAMBIA A TRANSFERENCIA</option>
	  <option value="ALERTA">ALERTA</option>
      
    </select>
  </div>
  </div>
  </div><br>
  <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->		
  <div class="row">
						  <div class="col-md-4"  style="display:none" id="banerPedidoTramiteS">
						  <div class="form-group">
    <label class="label-control">Fecha aproximada de llegada</label><br>
    <input type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegadaS" class="form-control"  value='<?php echo date('Y-m-d'); ?>'/>
</div>	
			
   </div>
   
   <div class="col-md-4" style="display:none" id="banerAlertaS">
   <div class="form-group">
    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
   
   </div>					  
  </div>
 <!-------------------------------------3----<<<<<<<<<<<<<<<<<<<<<-->	
 <div style="display:none" id="banerRotacionSemanaS">
 
 <div class="row" >
 <div class="col-md-4">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label>
    <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaS" name="rotacionxsemanaS">
      
  </div><br>
  </div> 
 </div>
 
 
 <div class="row">
	  
	  <div class="col-md-6">
	    <label for="exampleFormControlSelect1"><b>Saxenda (Caja  x 1)</b></label>
	  <div class="row">
	  
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja  x 1)</label>
    <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidadesCa1" name="CantidadUnidadesCa1" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio (Caja  x 1)</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="SaxendaCa1" name="SaxendaCa1">
      
  </div>
	  
	  </div>
	  
	  </div>
	    </div>
	  
	  
	  <div class="col-md-6">
	  <label for="exampleFormControlSelect1"><b>Saxenda (Caja  x 3)</b></label>
	  <div class="row">
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja  x 3)</label>
    <input type="number"  class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidadesCa3" name="CantidadUnidadesCa3" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio (Caja  x 3)</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="SaxendaCa3" name="SaxendaCa3" >
      
  </div>
	  </div>
	  </div>
	  </div>
	  
	  
	    </div>
  
  </div>
  	  
	   <!-------------------------------------4----<<<<<<<<<<<<<<<<<<<<<-->	
 <div class="row">
 <div class="col-md-4" style="display:none" id="banerCuentaDescuentosS">
  <label for="exampleFormControlSelect1" >Cuenta con descuentos</label>
	 <div class="form-check form-check-radio">
    <label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosS" id="CuentaDescuentosSiS" value="SI" >
        SI
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
	<label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosS" id="CuentaDescuentosNoS" value="NO" >
        NO
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
</div>
<div class="form-group" style="display:none;" id="banerSiDescuentoS">
<label for="exampleFormControlSelect1" >Descuento</label>
      <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoS" name="SiCuentaDescuantoS">
      <option value="" selected="" disabled="">Seleccione...</option>
      <option>Virtual</option>
      <option>Presencial</option>
	  <option>Mixto</option>
      
    </select>
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerPorcentajeS">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Porcentaje</label>
    <input type="number" maxlength="2" max="99" min='1' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeS">
      
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerObservacionPorcentajeS">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
    <textarea class="form-control" id="observacionS" rows="3"></textarea>
  </div>
 </div>
 <div class="col-md-4"></div>
 </div>						  
					 </div>
                      </div>
                    
                    </div>
					
					<!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---victoza------------>
					
					<div class="card-collapse" id="baner_victoza" >
                      <div class="card-header" role="tab" id="headingFor">
                        <h5 class="mb-0">
                          <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                            Victoza <span class="text-danger">*</span>
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseFor" class="collapse" role="tabpanel" aria-labelledby="headingFor" data-parent="#accordion">
                        <div class="card-body">
						

                          <div class="row">
						  <div class="col-md-4">
						  <div class="form-group">
    <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockV" name="CuentaCStockV">
      <option value=""  disabled="" selected="">Seleccione...</option>
      <option value="SI">SI</option>
      <option value="NO">NO</option>
      
    </select>
  </div>				
   </div>
						  
						  <div class="col-md-4" style="display:none;" id="banerPQnoStockV">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Porque no cuenta con stock  <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStockV" name="PorQNoStockV">
      <option value="" selected=""  disabled="">Seleccione...</option>
      <option value="NO LLEGA FORMULACION">NO LLEGA FORMULACI&Oacute;N</option>
      <option value="TRANSFERENCIA">TRANSFERENCIA</option>
	  <option value="AGOTADO">AGOTADO</option>
	   <option value="NO VECTORIZADO">NO VECTORIZADO</option>
      
    </select>
  </div>
  </div>
						  <div class="col-md-4" style="display:none;" id="banerMotAgotadoV">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Motivo de agotado</label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoV">
      <option  disabled="" value="" selected="">Seleccione...</option>
      <option value="PEDIDO EN TRAMITE">PEDIDO EN TR&Aacute;MITE</option>
      <option value="CAMBIA A TRANSFERENCIA">CAMBIA A TRANSFERENCIA</option>
	  <option value="ALERTA">ALERTA</option>
      
    </select>
  </div>
  </div>
  </div><br>
  <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->		
  <div class="row">
						  <div class="col-md-4"  style="display:none" id="banerPedidoTramiteV">
						  <div class="form-group">
    <label class="label-control">Fecha aproximada de llegada</label><br>
    <input type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegadaV" class="form-control"  value='<?php echo date('Y-m-d'); ?>'/>
</div>	
			
   </div>
   
   <div class="col-md-4" style="display:none" id="banerAlertaV">
   <div class="form-group">
    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
    <textarea class="form-control" id="rotacionxsemanaV" name="rotacionxsemanaV" rows="3"></textarea>
  </div>
   
   </div>					  
  </div>
 <!-------------------------------------3----<<<<<<<<<<<<<<<<<<<<<-->	
 <div style="display:none" id="banerRotacionSemanaV">
 <div class="row" >
 <div class="col-md-4">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label>
    <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaV" name="rotacionxsemanaV">
      
  </div><br>
  </div>
</div>

<div class="row">
	  
	  <div class="col-md-6">
	  <label for="exampleFormControlSelect1"><b>Victoza (Caja  x 1)</b></label>
	  <div class="row">
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja  x 1)</label>
    <input type="number"  class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidadesCaV1" name="CantidadUnidadesCaV1" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio (Caja  x 1)</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="VictozaCa1" name="VictozaCa1" >
      
  </div>
	  </div>
	  </div>
	  </div>
	  
	  <div class="col-md-6">
	    <label for="exampleFormControlSelect1"><b>Victoza  (Caja  x 2)</b></label>
	  <div class="row">
	  
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja  x 2)</label>
    <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidadesCaV2" name="CantidadUnidadesCaV2" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio (Caja  x 2)</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="VictozaCa2" name="VictozaCa2">
      
  </div>
	  
	  </div>
	  
	  </div>
	    </div>
	    </div>

 
  </div>
  	  
	   <!-------------------------------------4----<<<<<<<<<<<<<<<<<<<<<-->	
 <div class="row">
 <div class="col-md-4" style="display:none" id="banerCuentaDescuentosV">
  <label for="exampleFormControlSelect1" >Cuenta con descuentos</label>
	 <div class="form-check form-check-radio">
    <label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosV" id="CuentaDescuentosSiV" value="SI" >
        SI
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
	<label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosV" id="CuentaDescuentosNoV" value="NO" >
        NO
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
</div>
<div class="form-group" style="display:none;" id="banerSiDescuentoV">
<label for="exampleFormControlSelect1" >Descuento</label>
      <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoV" name="SiCuentaDescuantoV">
      <option value="" selected="" disabled="">Seleccione...</option>
      <option>Virtual</option>
      <option>Presencial</option>
	  <option>Mixto</option>
      
    </select>
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerPorcentajeV">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Porcentaje</label>
    <input type="number"  maxlength="2" max="99" min='1'  class="form-control selectpicker" data-style="btn btn-link" id="porcentajeV" name="porcentajeV" style="width:50px">
      
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerObservacionPorcentajeV">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
    <textarea class="form-control" id="observacionV" name="observacionV" rows="3"></textarea>
  </div>
 </div>
 <div class="col-md-4"></div>
 </div>						  
					 </div>
                      </div>
                    
                    
                    </div>
					
					<!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Ozempic------------>
					
				<div class="card-collapse" id="baner_ozempic" >
                      <div class="card-header" role="tab" id="headingFive">
                        <h5 class="mb-0">
                          <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Ozempic <span class="text-danger">*</span>
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
						

                          <div class="row">
						  <div class="col-md-4">
						  <div class="form-group">
    <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockO" name="CuentaCStockO">
      <option value=""  disabled="" selected="">Seleccione...</option>
      <option value="SI">SI</option>
      <option value="NO">NO</option>
      
    </select>
  </div>				
   </div>
						  
						  <div class="col-md-4" style="display:none;" id="banerPQnoStockO">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Porque no cuenta con stock  <span class="text-danger">*</span></label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStockO" name="PorQNoStockO">
      <option value="" selected=""  disabled="">Seleccione...</option>
      <option value="NO LLEGA FORMULACION">NO LLEGA FORMULACI&Oacute;N</option>
      <option value="TRANSFERENCIA">TRANSFERENCIA</option>
	  <option value="AGOTADO">AGOTADO</option>
	   <option value="NO VECTORIZADO">NO VECTORIZADO</option>
      
    </select>
  </div>
  </div>
						  <div class="col-md-4" style="display:none;" id="banerMotAgotadoO">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Motivo de agotado</label>
    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoO">
      <option  disabled="" value="" selected="">Seleccione...</option>
      <option value="PEDIDO EN TRAMITE">PEDIDO EN TR&Aacute;MITE</option>
      <option value="CAMBIA A TRANSFERENCIA">CAMBIA A TRANSFERENCIA</option>
	  <option value="ALERTA">ALERTA</option>
      
    </select>
  </div>
  </div>
  </div><br>
  <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->		
  <div class="row">
						  <div class="col-md-4"  style="display:none" id="banerPedidoTramiteO">
						  <div class="form-group">
    <label class="label-control">Fecha aproximada de llegada</label><br>
    <input type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegadaO" class="form-control"  value='<?php echo date('Y-m-d'); ?>'/>
</div>	
			
   </div>
   
   <div class="col-md-4" style="display:none" id="banerAlertaO">
   <div class="form-group">
    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
   
   </div>					  
  </div>
 <!-------------------------------------3----<<<<<<<<<<<<<<<<<<<<<-->	
 <div style="display:none" id="banerRotacionSemanaO">
 <div class="row" >
 <div class="col-md-4">
						    <div class="form-group">
    <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label>
    <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaO">
      
  </div><br>
  </div>
 </div>
 
 <div class="row">
	  
	  <div class="col-md-6">
	  <label for="exampleFormControlSelect1"><b>Ozempic 0.25mg-0.5mg</b></label>
	  <div class="row">
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades 0.25mg-0.5mg</label>
    <input type="number"  class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidadesCaO0" name="CantidadUnidadesCaO0" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio 0.25mg-0.5mg</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="Ozempic0" name="Ozempic0">
      
  </div>
	  </div>
	  </div>
	  </div>
	  
	  <div class="col-md-6">
	    <label for="exampleFormControlSelect1"><b>Ozempic 1mg</b></label>
	  <div class="row">
	  
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Cantidad Unidades 1mg</label>
    <input type="number"  class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidadesCaO1" name="CantidadUnidadesCaO1" style="width:50px">
      
  </div>
	  
	  </div>
	  <div class="col-md-6">
	    <div class="form-group">
    <label for="exampleFormControlSelect1">Precio 1mg</label>
    <input class="form-control selectpicker" data-style="btn btn-link" id="Ozempic0" name="Ozempic1">
      
  </div>
	  
	  </div>
	  
	  </div>
	    </div>
	    </div>
 
 
  </div>
  	  
	   <!-------------------------------------4----<<<<<<<<<<<<<<<<<<<<<-->	
 <div class="row">
 <div class="col-md-4" style="display:none" id="banerCuentaDescuentosO">
  <label for="exampleFormControlSelect1" >Cuenta con descuentos</label>
	 <div class="form-check form-check-radio">
    <label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosO" id="CuentaDescuentosSiO" value="SI" >
        SI
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
	<label class="form-check-label">
        <input class="form-check-input" type="radio" name="CuentaDescuentosO" id="CuentaDescuentosNoO" value="NO" >
        NO
        <span class="circle">
            <span class="check"></span>
        </span>
    </label>
</div>
<div class="form-group" style="display:none;" id="banerSiDescuentoO">
<label for="exampleFormControlSelect1" >Descuento</label>
      <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoO" name="SiCuentaDescuantoO">
      <option value="" selected="" disabled="">Seleccione...</option>
      <option>Virtual</option>
      <option>Presencial</option>
	  <option>Mixto</option>
      
    </select>
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerPorcentajeO">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Porcentaje</label>
    <input type="number" maxlength="2" max="99" min='1'  class="form-control selectpicker" data-style="btn btn-link" id="porcentajeO" name="porcentajeO">
      
  </div>
 </div>
 <div class="col-md-4" style="display:none" id="banerObservacionPorcentajeO">
 <div class="form-group">
    <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
    <textarea class="form-control" id="observacionO" name="observacionO" rows="3"></textarea>
  </div>
 </div>
 <div class="col-md-4"></div>
 </div>						  
					 </div>
                      </div>
                    
                    </div>
					
					<!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Compromiso------------>
					
					<div class="card-collapse" id="Compromiso" >
                      <div class="card-header" role="tab" id="headingSix">
                        <h5 class="mb-0">
                          <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Compromiso <span class="text-danger">*</span>
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
<div class="row">

<div class="col-md-6" >
 <div class="form-group">
    <label for="exampleFormControlSelect1">Texto</label>
    <textarea class="form-control" id="compromiso" name="compromiso" rows="3"></textarea>
  </div>
 </div>
 
</div>
<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4"><button formaction="../../../FUNCTIONS/CRUD/inserInfoFormRoutre.php?<?php echo md5('fit'); ?>=<?php echo md5(mt_rand()); ?>" type="submit" class="btn btn-success mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Guardar Informaci&oacute;n </button> </div>
<div class="col-md-4"></div>
</div>

  
 						  
					 </div>
                      </div>
                    </div>
					
					
                  </div>
                </div>
              </div>
		

        </div>
		</form>
      </div>
	  <footer class="footer">
        <div class="container-fluid">
          
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
          </div>
        </div>
      </footer>
    </div>
  </div>
	    <script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#RegisterValidation');
      setFormValidation('#TypeValidation');
      setFormValidation('#LoginValidation');
      setFormValidation('#RangeValidation');
    });
  </script>
  <!--   Core JS Files   -->
 
  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->

  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Tot"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar registros",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
  $('#myModal').modal(options)
  </script>
</body>

</html>