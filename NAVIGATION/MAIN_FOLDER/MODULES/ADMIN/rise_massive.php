<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>ruta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

      <div class="logo" align="center"><a href="#" class="simple-text logo-normal">
          <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li class="nav-item active ">
            <a class="nav-link" href="rise_massive.php">
              <i class="material-icons">content_paste</i>
              <p style="color:#FFFFFF">Asignar ruta</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="rutas_hoy.php">
              <i class="material-icons">library_books</i>
              <p>Ruta de Hoy</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sin_gestion.php">
              <i class="material-icons">bookmark_border</i>
              <p>Sin Gestion</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="reportes_encuesta.php">
            <i class="material-icons">dashboard</i>
              <p>Encuesta</p>
            </a>
          </li>


          <li class="nav-item active-pro ">
            <a class="nav-link" href="reportes.php">
              <i class="material-icons">dashboard</i>
              <p>Reportes</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link collapsed" data-toggle="collapse" href="#formsExamples" aria-expanded="false">
              <i class="material-icons">engineering</i>
              <p> Admin
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="formsExamples" style="">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="editar_adviser.php">
                    <i class="material-icons">person_search</i>
                    <span class="sidebar-normal"> Modificar Asesor </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="crear_adviser.php">
                    <i class="material-icons">person_add_alt_1</i>
                    <span class="sidebar-normal"> Agregar Asesor </span>
                  </a>
                </li>

              </ul>
            </div>
          </li>

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
            <a style="color:#333333;" class="navbar-brand" href="javascript:;">Cargue de rutero </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="container-fluid">
          <style type="text/css">
            .alert-info {
              color: #0c5460;
              background-color: #d1ecf1;
              border-color: #bee5eb;
            }

            .btn {
              color: black;
            }

            .card-header {
              background-color: #ffc107;
            }

            .btn-light:hover {
              color: black;
              background-color: #bee5eb;
              border-color: #bee5eb;
            }

            #agrupacion_n1 {
              border: 1px solid #ccc;
              margin-top: 1%;
              margin-bottom: 1%;
              padding: 1%;
            }

            #agrupacion_n2 {
              border: 1px solid #ccc;
              margin-top: 1%;
              margin-bottom: 1%;
              padding: 1%;
            }

            #subir_inventario {
              margin-top: 0.4%;
            }

            #nombre_archivo {
              padding: 7.5px;
              width: 100%;
            }

            #nombre_archivo:hover {
              background-color: #bee5eb;
            }
          </style>
          <!--alertas -->

          <script type="text/javascript">
            function subir_base_ajax() {

              var variable_recorer = "1"; //solo usa para activar el ajax

              $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/leer_excel.php',

                data: {
                  variable_recorer: variable_recorer
                },
                type: 'post',
                beforesend: function() {


                },

                success: function(data) {

                  var num_rows = $('#num_rows').val();

                  $('#return_data_subir_datos').html(data);

                }
              });
            }

            function calcular_registros_ajax() {

              var calculo = "1"; //solo usa para activar el ajax y devolver el valor

              $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/calcular_registros.php',

                data: {
                  calculo: calculo
                },
                type: 'post',
                beforesend: function() {


                },

                success: function(data) {
                  $('#retrurn_registros').html(data);

                }
              });
            }


            $(document).ready(function() {

              $('#cargar_base').click(function() {
                var rows_numero1 = $('#rows_numero').val();

                var rows_numero = rows_numero1 - 1;

                var tiempo_r = rows_numero * 60;

                var minutos = tiempo_r / 60000;

                activar_alerta(rows_numero, tiempo_r, minutos);

                subir_base_ajax();

                $("#descargar_plan").attr('disabled', true);

                $("#subir_inventario").attr('disabled', true);

                $("#calcular_registros").attr('disabled', true);

                $("#cargar_base").attr('disabled', true);

              });

              $('#calcular_registros').click(function() {

                calcular_registros_ajax();

                $("#cargar_base").attr('disabled', false);

              });
            });
          </script>
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->



            <div class="posicion" align="center">
              <div class="card border-dark mb-3" style="max-width: 97%;">
                <div class="card-header">
                  <h4>Asignar Rutas</h4>
                </div>
                <div class="card-body text-dark">
                  <h5 class="card-title">Descargar plantilla Asignacion de Ruta/ Adjuntar Ruta</h5>
                  <div class="alert alert-info" role="alert">
                    <p class="card-text"><strong><span class="fa fa-warning-circle"></span></strong> Instrucciones: Por favor descargar la plantilla de asignacion_ruta.xlsx . Llenar la informaci&oacute;n de ruta <b>sin cambiar la plantilla ni el encabezado</b>, luego adjuntar el archivo excel <b>sin cambiar el nombre ni la extensi&oacute;n del archivo</b></p>
                  </div>
                  <div class="row">
                    <div class="col-md-6" id="agrupacion_n1" name="agrupacion_n1">
                      <p></p>
                      <label id="nombre_archivo">asignacion_ruta.xlsx</label>
                      <form form id="FormData" name="FormData" content="text/html;" enctype="multipart/form-data" method="post">
                        <button class="btn btn-outline-success btn-lg btn-block" formaction="../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/exportable_masivo.php" name="descargar_plan" id="descargar_plan" type="submit"><span class="fa fa-cloud-download"></span>&nbsp;Descargar Plantilla</button>
                      </form>
                    </div>
                    <div class="col-md-6" id="agrupacion_n2" name="agrupacion_n2">
                      <form id="FormData" name="FormData" content="text/html;" enctype="multipart/form-data" method="post">
                        <div class="form-group form-file-upload form-file-multiple">
                          <input type="file" multiple="" class="inputFileHidden" name="id_excel" id="id_excel" accept=".xls,.xlsx">
                          <div class="input-group">
                            <input type="text" class="form-control inputFileVisible" placeholder="Adjunte el Archivo asignacion_ruta.xlsx" readonly="">
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-fab btn-round btn-info">
                                <i class="material-icons">attach_file</i>
                              </button>
                            </span>
                          </div>
                        </div>

                        <button class="btn btn-outline-warning btn-lg btn-block" name="subir_inventario" id="subir_inventario" type="submit"><span id="#"><i class="fa fa-paperclip" aria-hidden="true"></i></span>&nbsp;Guardar Archivo</button>
                      </form>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-12">
                      <?php

                      $tamano = $_FILES["id_excel"]['size'];
                      $tipo = $_FILES["id_excel"]['type'];
                      $archivo1 = $_FILES["id_excel"]['name'];
                      $error = $_FILES['id_excel']['error'];
                      $archivo1 = str_replace(" ", "_", $archivo1);
                      $prefijo1 = $archivo1;

                      if ($archivo1 != "") {
                        // guardamos el archivo a la carpeta files
                        $destino =  "../../FILES/" . $archivo1;

                        if (copy($_FILES['id_excel']['tmp_name'], $destino)) {
                          $status = "Archivo excel: <b>" . $archivo1 . "</b> se ha guardado.<br> Ahora calcular el numero de registros que tiene el archivo excel";
                          echo "<div class='alert alert-info' role='alert'>
                                <p class='card-text'>";
                          echo "<strong><span class='fa fa-info-circle'></span></strong>" . $status;
                          echo "</p>
                                </div>";
                          echo " <div class='form-group row'>
                                <div class='col-md-4'>
                                <button class='btn btn-outline-warning btn-lg btn-block' name='calcular_registros' id='calcular_registros' type='submit'><i class='fa fa-calculator' aria-hidden='true'></i>&nbsp;Calcular</button>

                                </div>
                                <div class='col-md-8'>
                                <button class='btn btn-outline-warning btn-lg btn-block' name='cargar_base' id='cargar_base' type='submit' disabled='true'><span class='fa fa-cloud-upload'></span>&nbsp;Subir Ruta</button>

                                </div>
                                </div>";

                          echo "<html>
                                <script></script> </html>";
                        } else {
                          echo "<div class='alert alert-info' role='alert'>
                                <p class='card-text'>";
                          $status = "Error al subir archivo";
                          echo $status;
                          echo "</p>
                                </div>";
                        }
                      }
                      ?>

                      <div id="retrurn_registros"></div>
                      <div id="return_data_subir_datos"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <script type="text/javascript">
            function activar_alerta(rows_numero, tiempo_r, minutos) {
              let timerInterval
              Swal.fire({
                title: ' Subiendo Base de datos masiva!',
                html: 'Subiedo ' + rows_numero + ' registros en ' + minutos + ' minutos y  <b></b> milliseconds.',
                timer: tiempo_r,
                timerProgressBar: true,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                      const b = content.querySelector('b')
                      if (b) {
                        b.textContent = Swal.getTimerLeft()
                      }
                    }
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval)
                }
              }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                  console.log('I was closed by the timer')

                }
              })
            }

            /*$(document).ready(function () {

            		$('#cargar_base').click(function ()
            		{

            				activar_alerta();

            		});

                $('#cantidad_r').click(function ()
                {
                    if (catidad_regis=='') {

                      Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<a href>Why do I have this issue?</a>'
            })

                    }else if (catidad_regis!='') {
                        $("#cargar_base").attr('disabled', false);
                    } 

                });*/
            /*function activar_alerta(tiempo_r){
  let timerInterval
  Swal.fire({
    title: ' Subiendo Base de datos masiva!',
    html: ' Subiedo registros <b></b> milliseconds.',
    timer: 5000,
    timerProgressBar: true,
    onBeforeOpen: () => {
      Swal.showLoading()
      timerInterval = setInterval(() => {
        const content = Swal.getContent()
        if (content) {
          const b = content.querySelector('b')
          if (b) {
            b.textContent = Swal.getTimerLeft()
          }
        }
      }, 100)
    },
    onClose: () => {
      clearInterval(timerInterval)
    }
  }).then((result) => { */
            /* Read more about handling dismissals below */
            /* if (result.dismiss === Swal.DismissReason.timer) {
      console.log('I was closed by the timer')
      alert(' Se va recargar la pagina por seguridad '+imageUrl+'si');
      
    }
  })

  //location.reload();

}
});*/
          </script>

        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      setFormValidation('#RegisterValidation');
      setFormValidation('#TypeValidation');
      setFormValidation('#LoginValidation');
      setFormValidation('#RangeValidation');
    });
  </script>
  <script>
    $(document).ready(function() {
      if ($('.card-header.card-chart').length != 0) {
        md.initDashboardPageCharts();
      }

      if ($('#websiteViewsChart').length != 0) {
        md.initDocumentationCharts();
      }


      if ($('.datetimepicker').length != 0) {
        md.initFormExtendedDatetimepickers();
      }
      if ($('#fullCalendar').length != 0) {
        md.initFullCalendar();
      }

      if ($('.slider').length != 0) {
        md.initSliders();
      }

      //  Activate the tooltips
      $('[data-toggle="tooltip"]').tooltip();

      // Activate Popovers
      $('[data-toggle="popover"]').popover();

      // Vector map
      if ($('#worldMap').length != 0) {
        md.initVectorMap();
      }

      if ($('#RegisterValidation').length != 0) {

        setFormValidation('#RegisterValidation');

        function setFormValidation(id) {
          $(id).validate({
            highlight: function(element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
              $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
            },
            success: function(element) {
              $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
              $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
            },
            errorPlacement: function(error, element) {
              $(element).closest('.form-group').append(error);
            },
          });
        }
      }

    });

    // FileInput
    $('.form-file-simple .inputFileVisible').click(function() {
      $(this).siblings('.inputFileHidden').trigger('click');
    });

    $('.form-file-simple .inputFileHidden').change(function() {
      var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
      $(this).siblings('.inputFileVisible').val(filename);
    });

    $('.form-file-multiple .inputFileVisible, .form-file-multiple .input-group-btn').click(function() {
      $(this).parent().parent().find('.inputFileHidden').trigger('click');
      $(this).parent().parent().addClass('is-focused');
    });

    $('.form-file-multiple .inputFileHidden').change(function() {
      var names = '';
      for (var i = 0; i < $(this).get(0).files.length; ++i) {
        if (i < $(this).get(0).files.length - 1) {
          names += $(this).get(0).files.item(i).name + ',';
        } else {
          names += $(this).get(0).files.item(i).name;
        }
      }
      $(this).siblings('.input-group').find('.inputFileVisible').val(names);
    });

    $('.form-file-multiple .btn').on('focus', function() {
      $(this).parent().siblings().trigger('focus');
    });

    $('.form-file-multiple .btn').on('focusout', function() {
      $(this).parent().siblings().trigger('focusout');
    });
  </script>
  <style>
    #ofBar {
      background: #de2e2e;
      text-align: left;
      z-index: 999999999;
      font-size: 16px;
      color: #fff;
      padding: 18px 5%;
      font-weight: 400;
      display: block;
      position: relative;
      top: 0px;
      box-shadow: 0 6px 13px -4px rgba(0, 0, 0, 0.25);
    }

    #ofBar b {
      font-size: 15px !important;
    }

    #count-down {
      display: initial;
      padding-left: 10px;
      font-weight: bold;
    }

    #close-bar {
      font-size: 22px;
      color: #3e3947;
      margin-right: 0;
      position: absolute;
      right: 5%;
      background: white;
      opacity: 0.5;
      padding: 0px;
      height: 25px;
      line-height: 21px;
      width: 25px;
      border-radius: 50%;
      text-align: center;
      top: 18px;
      cursor: pointer;
      z-index: 9999999999;
      font-weight: 200;
    }

    #close-bar:hover {
      opacity: 1;
    }

    #btn-bar {
      background-color: #fff;
      color: #40312d;
      border-radius: 4px;
      padding: 10px 20px;
      font-weight: bold;
      text-transform: uppercase;
      font-size: 12px;
      opacity: .95;
      margin-left: 15px;
      top: 0px;
      position: relative;
      cursor: pointer;
      text-align: center;
      box-shadow: 0 5px 10px -3px rgba(0, 0, 0, .23), 0 6px 10px -5px rgba(0, 0, 0, .25);
    }

    #btn-bar:hover {
      opacity: 0.9;
    }

    #btn-bar {
      opacity: 1;
    }

    #btn-bar span {
      color: red;
    }

    .right-side {
      float: right;
      margin-right: 60px;
      top: -6px;
      position: relative;
      display: block;
    }

    #oldPriceBar {
      text-decoration: line-through;
      font-size: 16px;
      color: #fff;
      font-weight: 400;
      top: 2px;
      position: relative;
    }

    #newPrice {
      color: #fff;
      font-size: 19px;
      font-weight: 700;
      top: 2px;
      position: relative;
      margin-left: 7px;
    }

    #fromText {
      font-size: 15px;
      color: #fff;
      font-weight: 400;
      margin-right: 3px;
      top: 0px;
      position: relative;
    }

    @media(max-width: 991px) {
      .right-side {
        float: none;
        margin-right: 0px;
        margin-top: 5px;
        top: 0px
      }

      #ofBar {
        padding: 50px 20px 20px;
        text-align: center;
      }

      #btn-bar {
        display: block;
        margin-top: 10px;
        margin-left: 0;
      }
    }

    @media (max-width: 768px) {
      #count-down {
        display: block;
        font-size: 25px;
      }
    }
  </style>
  <!--   Core JS Files   -->

  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->

  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Tot"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar registros",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>