<?php

	require 'LIBRERIA_PHP_EXCEL/Classes/PHPExcel.php';
	require 'LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()
	->setCreator('People Marketing')
	->setTitle('Excel en PHP')
	->setDescription('Documento')
	->setKeywords('excel phpexcel php')
	->setCategory('excel');

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('solicitudes');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'USUARIO');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', 'RUTA');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', 'FECHA_RUTA');
	$objPHPExcel->getActiveSheet()->setCellValue('D2', 'TEXTO(C2;"YYYY/mm/d")');
	

	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="asignacion_ruta.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');

?>
