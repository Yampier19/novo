<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Ver Ruta Asesor</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../DESIGN/CSS/estile_adicional.css" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/separador_miles.js"></script>
    <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/validaciones_form_route.js"></script>
    <script type="text/javascript">
        /*function iniciar(){
var boton=document.getElementById('obtener');
boton.addEventListener('click', obtener, false);
}*/
        function obtener() {
            var parametros = {
                enableHighAccuracy: true
            }
            navigator.geolocation.getCurrentPosition(mostrar, gestionarErrores, parametros);
        }

        function mostrar(posicion) {
            //var ubicacion=document.getElementById('localizacion');
            //var datos='';
            var latitud = posicion.coords.latitude;
            var longitud = posicion.coords.longitude;
            var exactitud = posicion.coords.accuracy;
            /*datos+='Latitud: '+posicion.coords.latitude+'<br>';
            datos+='Longitud: '+posicion.coords.longitude+'<br>';
            datos+='Exactitud: '+posicion.coords.accuracy+' metros.<br>';
            ubicacion.innerHTML=datos;*/
            document.getElementById("LAT").value = latitud;
            document.getElementById("LONG").value = longitud;
            /*swal(
            			{title: 'Bienvenido estamos usando GPS',
            		confirmButtonColor: '#17a2b8'});*/
        }

        function cerrar() {
            <?php
            //sleep(10);
            ?>
            window.onload = window.top.location.href = "../../CONNECTION/SECURITY/cerrar_sesion.php";
        }

        function gestionarErrores(error) {
            if (error.code == 1) {
                swal({
                    title: 'Debes permitir el uso de la geolocalizacion en tu navegador',
                    confirmButtonColor: '#17a2b8'
                }).then(function() {
                    window.top.location.href = "../../CONNECTION/SECURITY/cerrar_sesion.php";
                });
            }
        }

        <?php
        //if (isset($_GET['x'])) {
        ?>
        window.addEventListener('load', obtener, false);
        <?php
        //}
        ?>
    </script>

    <script type="text/javascript">
        $(document).ready(function separadordemiles() {
            function dir() {
                var titlePDV = $('titlePDV').val();
                var farmacia_pdv = $('#farmacia_pdv').val();

                $('#titlePDV').val(farmacia_pdv);

            }

            $('#farmacia_pdv').change(function() {
                dir();
            });

        });

        function ajustar() {
            var texto = document.getElementById("titlePDV");
            var txt = texto.value;
            var tamano = txt.length;
            tamano *= 10; //el valor multiplicativo debe cambiarse dependiendo del tama�o de la fuente
            texto.style.width = tamano + "px";
        }
    </script>

    <!--- validation--------->


</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="purple" data-background-color="rgba(0,188,212,.4)" data-image="../../../DESIGN/IMG/pagina-08.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

            <div class="logo" align="center"><a href="#" class="simple-text logo-normal">
                    <img src="../../../DESIGN/IMG/logo_novo.png" width="64px">People Tracking</a></div>
            <div class="sidebar-wrapper">
                <ul class="nav">

                    <li class="nav-item active ">
                        <a class="nav-link" href="table_route.php">
                            <i class="material-icons">content_paste</i>
                            <p style="color:#FFFFFF">Ruta de Hoy</p>
                        </a>
                    </li>
                    <!--  <li class="nav-item ">
            <a class="nav-link" href="./typography.html">
              <i class="material-icons">library_books</i>
              <p >Typography</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./icons.html">
              <i class="material-icons">bubble_chart</i>
              <p>Icons</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./map.html">
              <i class="material-icons">location_ons</i>
              <p>Maps</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./notifications.html">
              <i class="material-icons">notifications</i>
              <p>Notifications</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./rtl.html">
              <i class="material-icons">language</i>
              <p>RTL Support</p>
            </a>
          </li>
          <li class="nav-item active-pro ">
            <a class="nav-link" href="./upgrade.html">
              <i class="material-icons">unarchive</i>
              <p>Upgrade to PRO</p>
            </a>
          </li>-->
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">


                    <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
                        <a style="color:#333333;" class="navbar-brand" href="javascript:;">Ruta <?php date_default_timezone_set("America/Bogota");
                                                                                                $select_pdv = mysqli_query($conex, "SELECT * FROM `pdv_farmacia` WHERE `id_pdv` = '" . base64_decode($_GET[base64_encode('id_pdv')]) . "'");
                                                                                                while ($dato = mysqli_fetch_array($select_pdv)) {

                                                                                                    $id_pdv = $dato['id_pdv'];
                                                                                                    $cod_pdv =  $dato['cod_pdv'];
                                                                                                    $nombre_pdv =  $dato['nombre_pdv'];
                                                                                                    $ciudad_pdv = $dato['ciudad_pdv'];
                                                                                                    $direccion_pdv =  $dato['direccion_pdv'];
                                                                                                    $telefono =  $dato['telefono'];
                                                                                                    $cadena_pdv = $dato['cadena_pdv'];
                                                                                                    $canal_pdv = $dato['canal_pdv'];
                                                                                                    $nombre_regente = $dato['nombre_regente'];
                                                                                                    $num_regente = $dato['num_regetente'];
                                                                                                    $convenio = $dato['convenio'];
                                                                                                    $categoria = $dato['categoria'];
                                                                                                    $visita = $dato['visita'];
                                                                                                    $tipo_gestion = $dato['tipo_gestion'];
                                                                                                }

                                                                                                ?> <script>
                                var f = new Date();
                                document.write(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear());
                            </script></a> <?php if ($cod_pdv != '') {
                                                echo $cod_pdv;
                                            } else {
                                                echo 'N/A';
                                            } ?>&nbsp;&nbsp;&nbsp;
                    </div>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">

                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:;">
                                    <i class="material-icons">dashboard</i>
                                    <p class="d-lg-none d-md-block">
                                        Stats
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="d-lg-none d-md-block">
                                        Some Actions
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="d-lg-none d-md-block">
                                        Account
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                    <a class="dropdown-item" href="#">Perfil</a>
                                    <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="content">
                <?php
                $date = date('Y-m-d');
                $consul_ruta = mysqli_query($conex, "SELECT * FROM  pdv_efectivo WHERE DATE(fecha_registro) = '$date' AND `id_pdv` = '" . base64_decode($_GET[base64_encode('id_pdv')]) . "'");
                $conteo_efect = mysqli_num_rows($consul_ruta);

                if ($conteo_efect > 0) {

                    $tipo_visita = 'SI';

                    while ($datoEfec = mysqli_fetch_array($consul_ruta)) {

                        $cadena_frio = $datoEfec['cadena_frio'];
                        $observacion_nocadena_frio = $datoEfec['observacion_no_cadena_frio'];
                        $objetivoe = $datoEfec['objetivo'];
                        $compromisoe = $datoEfec['compromiso'];
                        $fecha_registro = $datoEfec['fecha_registro'];
                        $id_pdvR = $datoEfec['id_pdv'];
                        $id_foto = $datoEfec['id_foto'];
                    }

                    $consulta_ima = mysqli_query($conex, "SELECT * FROM `file_evidence` WHERE `id_file` = '$id_foto'");

                    while ($datoImg = mysqli_fetch_array($consulta_ima)) {

                        $nombre_fot = $datoImg['nombre_file'];
                        $url = $datoImg['link_file'];
                    }

                    $consulta_norditropin = mysqli_query($conex, "SELECT * FROM `norditropin` WHERE `id_pdv` = '$id_pdv' AND DATE(fecha_registro) = '$date'");

                    while ($datoEfecN = mysqli_fetch_array($consulta_norditropin)) {

                        $cuenta_stockN = $datoEfecN['cuenta_stock'];
                        $no_cuenta_stockN = $datoEfecN['no_cuenta_stock'];
                        $motivo_agotadoN = $datoEfecN['motivo_agotado'];
                        $fech_prox_llegadaN = $datoEfecN['fecha_prox_llegada'];
                        $alerta_observacionN = $datoEfecN['alerta_observacion'];
                        $rotacion_x_semanaN = $datoEfecN['rotacion_x_semana'];
                        $cantidad5gN = $datoEfecN['cantidad5g'];
                        $precio5gN = $datoEfecN['precio5g'];
                        $cantidad10gN = $datoEfecN['cantidad10g'];
                        $precio10gN = $datoEfecN['precio10g'];
                        $cantidad15gN = $datoEfecN['cantidad15g'];
                        $precio15gN = $datoEfecN['precio15g'];
                        $cuenta_descuentosN = $datoEfecN['cuenta_descuentos'];
                        $descuentoN = $datoEfecN['descuento'];
                        $porcentajeN = $datoEfecN['porcentaje'];
                        $observacion_nordN = $datoEfecN['observacion_nord'];
                    }

                    $consulta_saxenda = mysqli_query($conex, "SELECT * FROM `saxenda` WHERE `id_pdv` = '$id_pdvR'");

                    while ($datoEfecS = mysqli_fetch_array($consulta_saxenda)) {

                        $cuenta_stockS = $datoEfecS['cuenta_stock'];
                        $no_cuenta_stockS = $datoEfecS['no_cuenta_stock'];
                        $motivo_agotadoS = $datoEfecS['motivo_agotado'];
                        $fech_prox_llegadaS = $datoEfecS['fecha_prox_llegada'];
                        $alerta_observacionS = $datoEfecS['alerta_observacion'];
                        $rotacion_x_semanaS = $datoEfecS['rotacion_x_semana'];
                        $cantidad_cajax1S = $datoEfecS['cantidad_cajax1'];
                        $precio_cajax1S = $datoEfecS['precio_cajax1'];
                        $cantidad_cajax3S = $datoEfecS['precio_cajax3'];
                        $precio_cajax3S = $datoEfecS['precio_cajax3'];
                        $cuenta_descuentosS = $datoEfecS['cuenta_descuentos'];
                        $descuentoS = $datoEfecS['descuento'];
                        $porcentajeS = $datoEfecS['porcentaje'];
                        $observacion_nordS = $datoEfecS['observacion_nord'];
                    }

                    $consulta_victoza = mysqli_query($conex, "SELECT * FROM `victoza` WHERE `id_pdv`= '$id_pdvR'");

                    while ($datoEfecV = mysqli_fetch_array($consulta_victoza)) {

                        $cuenta_stockV = $datoEfecV['cuenta_stock'];
                        $no_cuenta_stockV = $datoEfecV['no_cuenta_stock'];
                        $motivo_agotado = $datoEfecV['motivo_agotado'];
                        $fech_prox_llegadaV = $datoEfecV['fecha_prox_llegada'];
                        $alerta_observacionV = $datoEfecV['alerta_observacion'];
                        $rotacion_x_semanV = $datoEfecV['rotacion_x_semana'];
                        $cantidad_cajax1V = $datoEfecV['cantidad_cajax1'];
                        $precio_cajax1V = $datoEfecV['precio_cajax1'];
                        $cantidad_cajax2V = $datoEfecV['precio_cajax2'];
                        $precio_cajax2V = $datoEfecV['precio_cajax2'];
                        $cuenta_descuentosV = $datoEfecV['cuenta_descuentos'];
                        $descuentoV = $datoEfecV['descuento'];
                        $porcentajeV = $datoEfecV['porcentaje'];
                        $observacion_nordV = $datoEfecV['observacion_nord'];
                    }

                    $consulta_ozempic = mysqli_query($conex, "SELECT * FROM `ozempic` WHERE `id_pdv` = '$id_pdvR'");

                    while ($datoEfecO = mysqli_fetch_array($consulta_ozempic)) {

                        $cuenta_stockO = $datoEfecO['cuenta_stock'];
                        $no_cuenta_stockO = $datoEfecO['no_cuenta_stock'];
                        $motivo_agotadoO = $datoEfecO['motivo_agotado'];
                        $fech_prox_llegadaO = $datoEfecO['fecha_prox_llegada'];
                        $alerta_observacionO = $datoEfecO['alerta_observacion'];
                        $rotacion_x_semanaO = $datoEfecO['rotacion_x_semana'];
                        $cantidad_0mgO = $datoEfecO['cantidad_0mg'];
                        $precio_0mgO = $datoEfecO['precio_0mg'];
                        $cantidad_1mgO = $datoEfecO['cantidad_1mg'];
                        $precio_1mgO = $datoEfecO['precio_1mg'];
                        $cuenta_descuentosO = $datoEfecO['cuenta_descuentos'];
                        $descuentoO = $datoEfecO['descuento'];
                        $porcentajeO = $datoEfecO['porcentaje'];
                        $observacion_nordO = $datoEfecO['observacion_nord'];
                    }
                } else {

                    $tipo_visita = 'NO';
                    $consul_ruta_no_efec = mysqli_query($conex, "SELECT * FROM  pdv_no_efectivo  WHERE DATE(fecha_registro) = '$date' AND id_pdv = '" . base64_decode($_GET[base64_encode('id_pdv')]) . "'");

                    while ($dato_no_efect = mysqli_fetch_array($consul_ruta_no_efec)) {

                        $causal_no_efectiva = $dato_no_efect['causal_no_efectiva'];
                        $observacion_no_efect = $dato_no_efect['observacion_no_efect'];
                        $id_foto = $dato_no_efect['id_foto'];
                    }

                    $consulta_ima = mysqli_query($conex, "SELECT * FROM `file_evidence` WHERE `id_file`= '$id_foto'");

                    while ($datoImg = mysqli_fetch_array($consulta_ima)) {

                        $nombre_fot = $datoImg['nombre_file'];
                        $url = $datoImg['link_file'];
                    }
                }



                ?>
                <form name="<?php echo sha1('form'); ?>" id="<?php echo sha1('form'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group" style="display:none">
                        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off" readonly value="" />
                        <input id="LONG" name="LONG" type="text" class="form-control" autocomplete="off" readonly value="" />
                    </div>
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header card-header-text card-header-primary">
                                <div class="card-text">
                                    <h4 class="card-title" style="width:auto;"><input onKeyUp="ajustar()" id="titlePDV" class="sinborde" value="<?php echo $nombre_pdv;  ?>" readonly="" /></h4>
                                </div>
                            </div>

                            <div class="card-body">
                                <div id="accordion" role="tablist">
                                    <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Informacion punto------------>
                                    <div class="card-collapse">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                                                    Informaci&oacute;n del Punto
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                            <!------->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label"><i class="fa fa-link" aria-hidden="true"></i> Cadena</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled"><input type="hidden" name="<?php echo crc32('id_pdv'); ?>" id="<?php echo crc32('id_pdv'); ?>" value="<?php echo base64_encode($id_pdv); ?>" />
                                                                    <input type="text" class="form-control" value="<?php echo $cadena_pdv; ?>" disabled="" id="cadena_pdv" name="cadena_pdv">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-4 col-form-label"><i class="fa fa-building-o" aria-hidden="true"></i> Ciudad</label>
                                                            <div class="col-sm-8">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $ciudad_pdv; ?>" disabled="" id="ciudad_pdv" name="ciudad_pdv">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> Tel&eacute;fono</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $telefono; ?>" id="telefono" name="telefono" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- 
					  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">
        <i class="fa fa-phone" aria-hidden="true"></i> Telefono
      </span>
    </div>
    <input type="text" class="form-control" value="3650097-3118127105" disabled="">
  </div>
					  --->
                                                    </div>


                                                </div>
                                                <!---------------- 2--->
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label"><i class="fa fa-home" aria-hidden="true"></i> Direcci&oacute;n</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $direccion_pdv; ?>" id="direccion_pdv" name="direccion_pdv" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label"><i class="fa fa-medkit" aria-hidden="true"></i> Farmacia</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $nombre_pdv; ?>" id="farmacia_pdv" name="farmacia_pdv" onKeyUp="ajustar()" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                                <!--------------- 3---->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label">Canal</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $canal_pdv; ?>" id="canal_pdv" name="canal_pdv" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-5 col-form-label"><i class="fa fa-user-circle" aria-hidden="true"></i> Nombre Regente</label>
                                                            <div class="col-sm-7">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $nombre_regente;  ?>" id="nombre_regente" name="nombre_regente" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-5 col-form-label">Numero regentes</label>
                                                            <div class="col-sm-7">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="number" min='1' max='10' class="form-control" value="<?php echo $num_regente; ?>" id="numero_regente" name="numero_regente">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <!----------4--------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-3 col-form-label">Convenio</label>
                                                            <div class="col-sm-9">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $convenio; ?>" id="convenio" name="convenio">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-5 col-form-label">Tipo de gesti&oacute;n</label>
                                                            <div class="col-sm-7">
                                                                <div class="form-group bmd-form-group is-filled">
                                                                    <input type="text" class="form-control" value="<?php echo $tipo_gestion; ?>" id="tipo_gestion" name="tipo_gestion">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <label class="col-sm-4 col-form-label">Tipo de Visita <span class="text-danger">*</span></label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control selectpicker" data-style="btn btn-info btn" title="Seleccione..." id="tipo_visita" name="tipo_visita" disabled="" selected="">
                                                                    <option disabled="" selected=""><?php echo $tipo_visita; ?></option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div><br>
                                                <!---------5----------->
                                                <div class="row">

                                                    <div class="col-md-4 col-sm-4">
                                                        <label for="exampleFormControlTextarea1"><i class="fa fa-camera" aria-hidden="true"></i> Evidencia del sitio </label><br>
                                                        <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail">
                                                                <img src="<?php echo $url; ?>" alt="...">
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                            <div>
                                                                <!-- <span class="btn btn-info btn-round btn-file">
                                                                    <span class="fileinput-new">Tomar Foto</span>
                                                                    <span class="fileinput-exists">Cambio</span>
                                                                    <input type="hidden" value="" name="..."><input type="file" name="evidencia_sitio" id="evidencia_sitio" selected="" disabled="">
                                                                    <div class="ripple-container"></div>
                                                                </span> -->
                                                                <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Eliminar<div class="ripple-container">
                                                                        <div class="ripple-decorator ripple-on ripple-out" style="left: 63.4063px; top: 15.1406px; background-color: rgb(255, 255, 255); transform: scale(15.5098);"></div>
                                                                    </div></a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 col-sm-6">

                                                        <?php if ($tipo_visita == 'SI') {  ?>
                                                            <div class="row" id="baner_cadena_frio">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlSelect1">Cadena de Fr&iacute;o <span class="text-danger">*</span></label>
                                                                        <select class="form-control selectpicker" data-style="btn btn-link" id="cadena_frio" name="cadena_frio">
                                                                            <option value="" selected="" disabled=""><?php echo $cadena_frio; ?></option>


                                                                        </select>
                                                                    </div>
                                                                </div><br>
                                                            </div>

                                                            <!---->
                                                            <?php if ($cadena_frio == 'NO') {  ?>
                                                                <div class="form-group" id="baner_observacion_cad_f">
                                                                    <label for="exampleFormControlTextarea1">Observaci&oacute;n</label><br>
                                                                    <textarea class="form-control" id="cadenaFrioObser" name="cadenaFrioObser" rows="3"><?php echo $observacion_nocadena_frio; ?></textarea>
                                                                </div><?php } else {
                                                                        echo '';
                                                                    }    ?>

                                                        <?php } else if ($tipo_visita == 'NO') {  ?>
                                                            <div class="row" id="baner_causal_no_efectiva">
                                                                <div class="col-md-8">
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlSelect1">Causal No Efectiva <span class="text-danger">*</span></label>
                                                                        <select class="form-control selectpicker" data-style="btn btn-link" id="causal_no_efectiva" name="causal_no_efectiva" selected="" disabled="">
                                                                            <option value=""><?php echo $causal_no_efectiva; ?></option>
                                                                        </select>
                                                                    </div>
                                                                </div><br>
                                                            </div>


                                                            <div class="form-group" id="baner_observacion_no_efectiva" style="display:none;">
                                                                <label for="exampleFormControlTextarea1">Observaci&oacute;n </label><br>
                                                                <textarea class="form-control" id="ObsCausalNoEfec" name="ObsCausalNoEfec" rows="3"><?php echo $observacion_no_efect; ?></textarea>
                                                            </div>

                                                        <?php } ?>


                                                        <div class="row">
                                                            <?php $select_info_efectivo = mysqli_query($conex, "SELECT * FROM `pdv_efectivo` WHERE `id_pdv` = '$id_pdv' ORDER BY `pdv_efectivo`.`id_efectivo` DESC LIMIT 1");

                                                            while ($datoE = mysqli_fetch_array($select_info_efectivo)) {

                                                                $objetivo = $datoE['objetivo'];
                                                                $compromiso = $datoE['compromiso'];
                                                            }

                                                            ?>

                                                            <button type="button" class="btn btn-round" data-toggle="modal" data-target="#signupModal">
                                                                <i class="material-icons">assignment</i>
                                                                Mas Informaci&oacute;n
                                                            </button>

                                                            <div class="row">
                        <div class="col-md-4"></div>
                        
                    </div>


                                                            <div class="modal fade" id="signupModal" tabindex="-1" role="dialog">
                                                                <div class="modal-dialog modal-signup" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="card card-signup card-plain">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title card-title"><?php echo $nombre_pdv; ?></h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <i class="material-icons">clear</i>
                                                                                </button>
                                                                            </div>

                                                                            <!--------------------------------------------------------------------------------------------------------------------------->

                                                                            <div class="modal-body">

                                                                                <div class="row">
                                                                                    <!------------------------------------------------------------------------------------------------------------------------>
                                                                                    <div class="col-md-5 ml-auto">
                                                                                        <div class="info info-horizontal">
                                                                                            <div class="icon icon-rose">
                                                                                                <i class="material-icons">timeline</i>
                                                                                            </div>
                                                                                            <div class="description">
                                                                                                <h4 class="info-title">Compromiso</h4>
                                                                                                <p class="description">
                                                                                                    <?php echo $compromiso; ?>
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!------------------------------------------------------------------------------------------------------------------------>
                                                                                    <div class="col-md-5 mr-auto">
                                                                                        <div class="info info-horizontal">
                                                                                            <div class="icon icon-primary">
                                                                                                <i class="material-icons">code</i>
                                                                                            </div>
                                                                                            <div class="description">
                                                                                                <h4 class="info-title">Objetivo</h4>
                                                                                                <p class="description" id="verObj">
                                                                                                    <?php echo $objetivo; ?>
                                                                                                </p>
                                                                                                <p class="description" id="editarObj" style="display:none"><textarea class="form-control" name="objetivo" id="objetivo"><?php echo $objetivo; ?></textarea></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!------------------------------------------------------------------------------------------------------------------------>
                                                                                </div>

                                                                                <!-- <div class="row">
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label">
                                                                                            <input class="form-check-input" type="checkbox" value="" checked>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            Seleccione si desea Modificar el <a style="color:#00bcd4">Objetivo</a>.
                                                                                        </label>
                                                                                    </div>
                                                                                </div> -->
                                                                                <div class="modal-footer justify-content-center">
                                                                                    <a class="btn btn-info btn-round" data-dismiss="modal" aria-label="Close" style="color:#FFFFFF;">Volver</a>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><br>

                                                            <!-- <hr>
                                                            <div class="row">
                                                                <div class="col-md-4"></div>
                                                                <div class="col-md-5"><button type="button" onclick=" location.href='table_route.php'" class="btn btn-danger mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Volver </button> </div>
                                                                <div class="col-md-4"></div>
                                                            </div> -->


                                                            <!------------------------------------------------------------------------------------------------------------------------>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <?php if ($tipo_visita == 'SI') {  ?>
                                        <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---VICTOZA----------->


                                        <div class="card-collapse" id="baner_norditropin">
                                            <div class="card-header" role="tab" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <b> Victoza <span class="text-danger">*</span></b>
                                                </h5>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
                                                        <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockV" name="CuentaCStockV" disabled="" selected="">
                                                            <option value=""><?php echo $cuenta_stockV; ?></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php if ($cuenta_stockV == 'SI') { ?>

                                            </div>
                                            <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label><br>
                                                        <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaV" name="rotacionxsemanaV" value="<?php echo $rotacion_x_semanV; ?>" selected="" disabled="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="exampleFormControlSelect1"><b>Victoza (Caja x 1)</b></label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Cantidad Unidades (Caja x 1)</label>
                                                                <input type="number" class="form-control selectpicker" maxlength="3" max="300" min='0' data-style="btn btn-link" id="CantidadUnidadesCaV1" name="CantidadUnidadesCaV1" style="width:50px" value="<?php echo $cantidad_cajax1V; ?>" selected="" disabled="">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Precio (Caja x 1)</label>
                                                                <input class="form-control selectpicker" data-style="btn btn-link" id="VictozaCa1" name="VictozaCa1" type="tel" value="<?php echo $precio_cajax1V; ?>" selected="" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="exampleFormControlSelect1"><b>Victoza (Caja x 2)</b></label>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Cantidad Unidades (Caja x 2)</label>
                                                                <input type="number" maxlength="3" max="300" min='0' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidadesCaV2" name="CantidadUnidadesCaV2" style="width:50px" value="<?php echo $cantidad_cajax2V; ?>" selected="" disabled="">

                                                            </div>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Precio (Caja x 2)</label>
                                                                <input class="form-control selectpicker" data-style="btn btn-link" id="VictozaCa2" name="VictozaCa2" type="tel" value="<?php echo $precio_cajax2V; ?>" selected="" disabled="">

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <!------------------------------------------ 3 ------------------------------>

                                            <div class="row">
                                                <div class="col-md-4" id="banerCuentaDescuentosV">
                                                    <label for="exampleFormControlSelect1">Cuenta con descuentos</label>
                                                    <div class="form-check form-check-radio" id="CuentaDescuentosV">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="cuentaDescuentosV" id="CuentaDescuentosSiV" value="SI" <?php if ($cuenta_descuentosV == 'SI') {
                                                                                                                                                                            echo "checked";
                                                                                                                                                                        }; ?> disabled="" selected="">
                                                            SI
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="cuentaDescuentosV" id="CuentaDescuentosNoV" value="NO" <?php if ($cuenta_descuentosV == 'NO') {
                                                                                                                                                                            echo "checked";
                                                                                                                                                                        }; ?> disabled="" selected="">
                                                            NO
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <?php if ($cuenta_descuentosV == 'SI') {  ?>
                                                        <div class="form-group" id="banerSiDescuentoV">
                                                            <label for="exampleFormControlSelect1">Descuento</label>
                                                            <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoV" name="SiCuentaDescuantoV" selected="" disabled="">
                                                                <option value=""><?php echo $descuentoV; ?></option>
                                                            </select>
                                                        </div>

                                                    <?php } else if ($cuenta_descuentosV == 'NO') { ?>
                                                        <div class="form-group" id="banerSiDescuentoV" style="display: none">
                                                            <label for="exampleFormControlSelect1">Descuento</label>
                                                            <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoV" name="SiCuentaDescuantoV">
                                                                <option value=""><?php echo $descuentoV; ?></option>
                                                            </select>
                                                        </div>
                                                    <?php } ?>

                                                </div>
                                                <?php if ($cuenta_descuentosV == 'SI') {  ?>
                                                    <div class="col-md-4" id="banerPorcentajeV">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlSelect1">Porcentaje</label>
                                                            <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeV" value="<?php echo $porcentajeV; ?>" selected="" disabled="">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="banerObservacionPorcentajeV">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                                            <textarea class="form-control" id="observacionV" rows="3"><?php echo $observacion_nordV; ?></textarea selected="" disabled="">
                                                            </div>
                                                        </div>
                                                    <?php } else if ($cuenta_descuentosV == 'NO') { ?>
                                                        <div class="col-md-4" id="banerPorcentajeV" style="display: none">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Porcentaje</label>
                                                                <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeV" value="<?php echo $porcentajeV; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4" id="banerObservacionPorcentajeV" style="display: none">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                                                <textarea class="form-control" id="observacionV" rows="3"><?php echo $observacion_nordV; ?></textarea>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="col-md-4"></div>


                                        </div>

                                    <?php } else { ?>
                                        <div class="col-md-4" id="banerPQnoStockV">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Porque no cuenta con stock <span class="text-danger">*</span></label>
                                                <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStockV" name="PorQNoStockV" selected="" disabled="">
                                                    <option value=""><?php echo $no_cuenta_stockV; ?></option>
                                                </select>
                                            </div>
                                        </div><br><br>
                                        <!-- 'NO LLEGA FORMULACION' or 'TRANSFERENCIA' or 'NO VECTORIZADO' -->
                                        <?php if ($no_cuenta_stockV == 'AGOTADO') { ?>
                                            <div class="col-md-4" id="banerMotAgotadoV">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoV" name="MotivoAgotadoV" selected="" disabled="">
                                                        <option value=""><?php echo $motivo_agotado; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else if ($no_cuenta_stockV == 'NO LLEGA FORMULACION') { ?>
                                            <div class="col-md-4" id="banerMotAgotadoV" style="display: none;">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoV" name="MotivoAgotadoV" selected="" disabled="">
                                                        <option value=""><?php echo $motivo_agotado; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else if ($no_cuenta_stockV == 'TRANSFERENCIA') { ?>
                                            <div class="col-md-4" id="banerMotAgotadoV" style="display: none;">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoV" name="MotivoAgotadoV" selected="" disabled="">
                                                        <option value=""><?php echo $motivo_agotado; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else if ($no_cuenta_stockV == 'NO VECTORIZADO') { ?>
                                            <div class="col-md-4" id="banerMotAgotadoV" style="display: none;">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoV" name="MotivoAgotadoV" selected="" disabled="">
                                                        <option value=""><?php echo $motivo_agotado; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <?php if ($motivo_agotado == 'PEDIDO EN TRAMITE') { ?>
                                            <div class="col-md-4" id="banerPedidoTramiteV">
                                                <div class="form-group">
                                                    <label class="label-control">Fecha aproximada de llegada</label><br>
                                                    <input type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegadaV" name="fechaApoxiLlegadaV" class="form-control" value='<?php echo $fech_prox_llegadaV; ?>' / selected="" disabled="">
                                                </div>
                                            </div>

                                        <?php } else if ($motivo_agotado == 'ALERTA') { ?>
                                            <div class="col-md-4" id="banerAlertaV">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
                                                    <textarea class="form-control" id="alerta_observacionV" name="alerta_observacionV" rows="3" selected="" disabled=""><?php echo $alerta_observacionV; ?></textarea>
                                                </div>
                                            </div><br>
                                        <?php } else { ?>
                                        <?php } ?>




                                        <div class="col-md-4"></div>


                                </div>
                            <?php } ?>

                            <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---SAXENDA----------->

                            <div class="card-collapse" id="baner_norditropin">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h5 class="mb-0">
                                        <b> Saxenda <span class="text-danger">*</span></b>
                                    </h5>
                                </div>




                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
                                            <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockS" name="CuentaCStockS" disabled="" selected="">
                                                <option value=""><?php echo $cuenta_stockS; ?></option>
                                            </select>
                                        </div>
                                    </div>


                                    <?php if ($cuenta_stockS == 'SI') { ?>
                                </div>

                                <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label><br>
                                            <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaS" name="rotacionxsemanaS" value="<?php echo $rotacion_x_semanaS; ?>" disabled="" selected="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <label for="exampleFormControlSelect1"><b>Saxenda (Caja x 1)</b></label>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja x 1)</label>
                                                    <input type="number" maxlength="3" max="300" min='0' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidadesCa1" name="CantidadUnidadesCa1" style="width:50px" value="<?php echo $cantidad_cajax1S; ?>" disabled="" selected="">
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Precio (Caja x 1)</label>
                                                    <input class="form-control selectpicker" data-style="btn btn-link" id="SaxendaCa1" name="SaxendaCa1" type="tel" value="<?php echo $precio_cajax1S; ?>" disabled="" selected="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <label for="exampleFormControlSelect1"><b>Saxenda (Caja x 3)</b></label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Cantidad Unidades (Caja x 3)</label>
                                                    <input type="number" class="form-control selectpicker" maxlength="3" max="300" min='0' data-style="btn btn-link" id="CantidadUnidadesCa3" name="CantidadUnidadesCa3" style="width:50px" value="<?php echo $cantidad_cajax3S; ?>" disabled="" selected="">
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Precio (Caja x 3)</label>
                                                    <input class="form-control selectpicker" data-style="btn btn-link" id="SaxendaCa3" name="SaxendaCa3" type="tel" value="<?php echo $precio_cajax3S; ?>" disabled="" selected="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <!------------------ SAXENDA USAR ------------------>
                                <div class="row">
                                    <div class="col-md-4" id="banerCuentaDescuentos">
                                        <label for="exampleFormControlSelect1">Cuenta con descuentos</label>
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="CuentaDescuentosS" id="CuentaDescuentosSiS" value="SI" <?php if ($cuenta_descuentosS == 'SI') {
                                                                                                                                                                echo "checked";
                                                                                                                                                            }; ?> disabled="" selected="">
                                                SI
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="CuentaDescuentosS" id="CuentaDescuentosNoS" value="NO" <?php if ($cuenta_descuentosS == 'NO') {
                                                                                                                                                                echo "checked";
                                                                                                                                                            }; ?> disabled="" selected="">
                                                NO
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <?php if ($cuenta_descuentosS == 'SI') {  ?>
                                            <div class="form-group" id="banerSiDescuentoS">
                                                <label for="exampleFormControlSelect1">Descuento</label>
                                                <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoS" name="SiCuentaDescuantoS" disabled="" selected="">
                                                    <option value=""><?php echo $descuentoS; ?></option>
                                                </select>
                                            </div>

                                        <?php } else if ($cuenta_descuentosS == 'NO') { ?>
                                            <div class="form-group" id="banerSiDescuentoS" style="display: none;">
                                                <label for="exampleFormControlSelect1">Descuento</label>
                                                <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoS" name="SiCuentaDescuantoS" disabled="" selected="">
                                                    <option value=""><?php echo $descuentoS; ?></option>
                                                </select>
                                            </div>
                                        <?php } ?>

                                    </div>
                                    <?php if ($cuenta_descuentosS == 'SI') {  ?>
                                        <div class="col-md-4" id="banerPorcentajeS">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Porcentaje</label>
                                                <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeS" value="<?php echo $porcentajeS; ?>" disabled="" selected="">
                                            </div>
                                        </div>

                                        <div class="col-md-4" id="banerObservacionPorcentajeS">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                                <textarea class="form-control" id="observacionS" rows="3" disabled="" selected=""><?php echo $observacion_nordS; ?></textarea>
                                            </div>
                                        </div>
                                    <?php } else if ($cuenta_descuentosS == 'NO') { ?>
                                        <div class="col-md-4" id="banerPorcentajeS" style="display: none;">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Porcentaje</label>
                                                <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeS" value="<?php echo $porcentajeS; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-4" id="banerObservacionPorcentajeS" style="display: none;">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                                <textarea class="form-control" id="observacionS" rows="3"><?php echo $observacion_nordS; ?></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-4"></div>
                            </div>

                        <?php } else { ?>
                            <div class="col-md-4" id="banerPQnoStock">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Porque no cuenta con stock <span class="text-danger">*</span></label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStock" name="PorQNoStock" selected="" disabled="">
                                        <option value=""><?php echo $no_cuenta_stockS; ?></option>
                                    </select>
                                </div>
                            </div><br><br>
                            <!-- 'NO LLEGA FORMULACION' or 'TRANSFERENCIA' or 'NO VECTORIZADO' -->
                            <?php if ($no_cuenta_stockS == 'AGOTADO') { ?>
                                <div class="col-md-4" id="banerMotAgotado">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoS" name="MotivoAgotadoS" disabled="" selected="">
                                            <option value=""><?php echo $motivo_agotadoS; ?></option>
                                        </select>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="col-md-4" id="banerMotAgotado" style="display: none;">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado" disabled="" selected="">
                                            <option value=""><?php echo $motivo_agotadoS; ?></option>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>


                            <?php if ($motivo_agotadoS == 'PEDIDO EN TRAMITE') { ?>
                                <div class="col-md-4" id="banerPedidoTramite">
                                    <div class="form-group">
                                        <label class="label-control">Fecha aproximada de llegada</label><br>
                                        <input disabled="" selected="" type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegada" name="fechaApoxiLlegada" class="form-control" value='<?php echo $fech_prox_llegadaS; ?>' />
                                    </div>
                                </div>

                            <?php } else if ($motivo_agotadoS == 'ALERTA') { ?>
                                <div class="col-md-4" id="banerAlerta">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
                                        <textarea class="form-control" id="alerta_observacion" name="alerta_observacion" rows="3" disabled="" selected=""><?php echo $alerta_observacionS; ?></textarea>
                                    </div>
                                </div><br>
                            <?php } ?>

                            <div class="col-md-4"></div>


                            </div>
                        <?php } ?>


                        <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Norditropin------------>

                        <div class="card-collapse" id="baner_norditropin">
                            <div class="card-header" role="tab" id="headingTwo">
                                <h5 class="mb-0">
                                    <b> Norditropin <span class="text-danger">*</span></b>
                                </h5>
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStock" name="CuentaCStock" disabled="" selected="">
                                            <option value=""><?php echo $cuenta_stockN; ?></option>
                                        </select>
                                    </div>
                                </div>


                                <?php if ($cuenta_stockN == 'SI') { ?>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label><br>
                                        <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemana" name="rotacionxsemana" value="<?php echo $rotacion_x_semanaN; ?>" disabled="" selected="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1"><b>Norditropin 5mg</b></label>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Cantidad Unidades 5mg</label>
                                        <input type="number" class="form-control selectpicker" maxlength="3" max="300" min='1' data-style="btn btn-link" id="CantidadUnidades5" name="CantidadUnidades5" style="width:50px" value="<?php echo $cantidad5gN; ?>" disabled="" selected="">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1">Precio 5mg</label></label>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1"><b>
                                                <p> </p>
                                            </b></label>
                                        <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin5p" style="width:60px" name="Norditropin5p" value="<?php echo $precio5gN; ?>" disabled="" selected="">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1"><b>Norditropin 10mg</b></label>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Cantidad Unidades 10mg</label>
                                        <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidades10" name="CantidadUnidades10" style="width:150px" value="<?php echo $cantidad10gN; ?>" disabled="" selected="">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1"><b>
                                            <p> </p>
                                        </b></label>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Precio 10gm</label>
                                        <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin10p" name="Norditropin10p" value="<?php echo $precio10gN; ?>" disabled="" selected="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label for="exampleFormControlSelect1"><b>Norditropin 15mg</b></label>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Cantidad Unidades 15mg</label>
                                        <input type="number" maxlength="3" max="300" min='1' class="form-control selectpicker" data-style="btn btn-link" id="CantidadUnidades15" name="CantidadUnidades15" style="width:50px" value="<?php echo $cantidad15gN; ?>" disabled="" selected="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Precio 15mg</label>
                                        <input class="form-control selectpicker" data-style="btn btn-link" id="Norditropin15p" name="Norditropin15p" value="<?php echo $precio15gN; ?>" disabled="" selected="">
                                    </div>
                                </div>
                            </div>

                            <!------------------ NORDITROPIIIIIIIIIIIN USAR ------------------>
                            <div class="row">
                                <div class="col-md-4" id="banerCuentaDescuentos">
                                    <label for="exampleFormControlSelect1">Cuenta con descuentos</label>
                                    <div class="form-check form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="CuentaDescuentos" id="CuentaDescuentosSi" value="SI" <?php if ($cuenta_descuentosN == 'SI') {
                                                                                                                                                        echo "checked";
                                                                                                                                                    }; ?> disabled="" selected="">
                                            SI
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="CuentaDescuentos" id="CuentaDescuentosNo" value="NO" <?php if ($cuenta_descuentosN == 'NO') {
                                                                                                                                                        echo "checked";
                                                                                                                                                    }; ?> disabled="" selected="">
                                            NO
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <?php if ($cuenta_descuentosN == 'SI') {  ?>
                                        <div class="form-group" id="banerSiDescuento">
                                            <label for="exampleFormControlSelect1">Descuento</label>
                                            <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuanto" name="SiCuentaDescuanto" disabled="" selected="">
                                                <option value=""><?php echo $descuentoN; ?></option>
                                            </select>
                                        </div>

                                    <?php } else if ($cuenta_descuentosN == 'NO') { ?>
                                        <div class="form-group" id="banerSiDescuento" style="display:none;">
                                            <label for="exampleFormControlSelect1">Descuento</label>
                                            <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuanto" name="SiCuentaDescuanto" disabled="" selected="">
                                                <option value=""><?php echo $descuentoN; ?></option>
                                            </select>
                                        </div>
                                    <?php } ?>

                                </div>
                                <?php if ($cuenta_descuentosN == 'SI') {  ?>
                                    <div class="col-md-4" id="banerPorcentaje">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Porcentaje</label>
                                            <input type="number" maxlength="2" max="99" min='1' class="form-control selectpicker" data-style="btn btn-link" name="porcentaje" id="porcentaje" style="width:30px" value="<?php echo $porcentajeN; ?>" disabled="" selected="">

                                        </div>
                                    </div>
                                    <div class="col-md-4" id="banerObservacionPorcentaje">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="observacionN" id="observacionN" disabled="" selected=""><?php echo $observacion_nordN; ?></textarea>
                                        </div>
                                    </div>
                                <?php } else if ($cuenta_descuentosN == 'NO') { ?>
                                    <div class="col-md-4" id="banerPorcentaje" style="display: none;">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Porcentaje</label>
                                            <input type="number" maxlength="2" max="99" min='1' class="form-control selectpicker" data-style="btn btn-link" name="porcentaje" id="porcentaje" style="width:50px" value="<?php echo $porcentajeN; ?>">

                                        </div>
                                    </div>
                                    <div class="col-md-4" id="banerObservacionPorcentaje" style="display: none;">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="observacionN" id="observacionN"><?php echo $observacion_nordN; ?></textarea>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-4"></div>


                        </div>

                    <?php } else { ?>
                        <div class="col-md-4" id="banerPQnoStock">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Porque no cuenta con stock <span class="text-danger">*</span></label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStock" name="PorQNoStock" selected="" disabled="">
                                    <option value=""><?php echo $no_cuenta_stockN; ?></option>
                                </select>
                            </div>
                        </div><br><br>

                        <?php if ($no_cuenta_stockN == 'NO LLEGA FORMULACION') { ?>
                            <div class="col-md-4" id="banerMotAgotado" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado" disabled="" selected="">
                                        <option value=""><?php echo $motivo_agotadoN; ?></option>
                                    </select>
                                </div>
                            </div>
                        <?php } else if ($no_cuenta_stockN == 'TRANSFERENCIA') { ?>
                            <div class="col-md-4" id="banerMotAgotado" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado" disabled="" selected="">
                                        <option value=""><?php echo $motivo_agotadoN; ?></option>
                                    </select>
                                </div>
                            </div>
                        <?php } else if ($no_cuenta_stockN == 'NO VECTORIZADO') { ?>
                            <div class="col-md-4" id="banerMotAgotado" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado" disabled="" selected="">
                                        <option value=""><?php echo $motivo_agotadoN; ?></option>
                                    </select>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-4" id="banerMotAgotado">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotado" name="MotivoAgotado" disabled="" selected="">
                                        <option value=""><?php echo $motivo_agotadoN; ?></option>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>


                        <?php if ($motivo_agotadoN == 'PEDIDO EN TRAMITE') { ?>
                            <div class="col-md-4" id="banerPedidoTramite">
                                <div class="form-group">
                                    <label class="label-control">Fecha aproximada de llegada</label><br>
                                    <input disabled="" selected="" type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegada" name="fechaApoxiLlegada" class="form-control" value='<?php echo $fech_prox_llegadaN; ?>' />
                                </div>
                            </div>

                        <?php } else if ($motivo_agotadoN == 'ALERTA') { ?>
                            <div class="col-md-4" id="banerAlerta">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
                                    <textarea class="form-control" id="alerta_observacion" name="alerta_observacion" rows="3" disabled="" selected=""><?php echo $alerta_observacionN; ?></textarea>
                                </div>
                            </div><br>
                        <?php } ?>

                        <div class="col-md-4"></div>


                        </div>
                    <?php } ?>


                    <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---OZEMPIC----------->

                    <div class="card-collapse" id="baner_norditropin">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0">
                                <b> Ozempic <span class="text-danger">*</span></b>
                            </h5>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Cuenta con el Stock <span class="text-danger">*</span></label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="CuentaCStockO" name="CuentaCStockO" disabled="" selected="">
                                        <option value=""><?php echo $cuenta_stockO; ?></option>
                                    </select>
                                </div>
                            </div>


                            <?php if ($cuenta_stockO == 'SI') { ?>
                        </div>

                        <!-------------------------------------2----<<<<<<<<<<<<<<<<<<<<<-->

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Rotaci&oacute;n de la semana</label><br>
                                    <input type="number" class="form-control selectpicker" data-style="btn btn-link" id="rotacionxsemanaO" name="rotacionxsemanaO" value="<?php echo $rotacion_x_semanaO; ?>" disabled="" selected="">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <label for="exampleFormControlSelect1"><b>Ozempic 0.25mg-0.5mg</b></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Cant. Unid. 0.25mg-0.5mg</label>
                                            <input type="number" class="form-control selectpicker" maxlength="3" max="300" min='0' data-style="btn btn-link" id="CantidadUnidadesCaO0" name="CantidadUnidadesCaO0" style="width:50px" value="<?php echo $cantidad_0mgO; ?>" disabled="" selected="">
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Precio 0.25mg-0.5mg</label>
                                            <input class="form-control selectpicker" data-style="btn btn-link" id="Ozempic0" name="Ozempic0" type="tel" value="<?php echo $precio_0mgO; ?>" disabled="" selected="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="exampleFormControlSelect1"><b>Ozempic 1mg</b></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Cantidad Unidades 1mg</label>
                                            <input type="number" class="form-control selectpicker" maxlength="3" max="300" min='0' data-style="btn btn-link" id="CantidadUnidadesCaO1" name="CantidadUnidadesCaO1" style="width:50px" value="<?php echo $cantidad_1mgO; ?>" disabled="" selected="">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Precio 1mg</label>
                                            <input class="form-control selectpicker" data-style="btn btn-link" id="Ozempic1" name="Ozempic1" type="tel" value="<?php echo $precio_1mgO; ?>" disabled="" selected="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!------------------------------------------ 3 ------------------------------>

                        <div class="row">
                            <div class="col-md-4" id="banerCuentaDescuentos">
                                <label for="exampleFormControlSelect1">Cuenta con descuentos</label>
                                <div class="form-check form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="CuentaDescuentosO" id="CuentaDescuentosSiO" value="SI" <?php if ($cuenta_descuentosO == 'SI') {
                                                                                                                                                        echo "checked";
                                                                                                                                                    }; ?> disabled="" selected="">
                                        SI
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="CuentaDescuentosO" id="CuentaDescuentosNoO" value="NO" <?php if ($cuenta_descuentosO == 'NO') {
                                                                                                                                                        echo "checked";
                                                                                                                                                    }; ?> disabled="" selected="">
                                        NO
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <?php if ($cuenta_descuentosO == 'SI') {  ?>
                                    <div class="form-group" id="banerSiDescuentoO">
                                        <label for="exampleFormControlSelect1">Descuento</label>
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoO" name="SiCuentaDescuantoO" disabled="" selected="">
                                            <option value=""><?php echo $descuentoO; ?></option>
                                        </select>
                                    </div>

                                <?php } else if ($cuenta_descuentosO == 'NO') { ?>
                                    <div class="form-group" id="banerSiDescuentoO" style="display: none">
                                        <label for="exampleFormControlSelect1">Descuento</label>
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="SiCuentaDescuantoO" name="SiCuentaDescuantoO" disabled="" selected="">
                                            <option value=""><?php echo $descuentoO; ?></option>
                                        </select>
                                    </div>
                                <?php } ?>

                            </div>
                            <?php if ($cuenta_descuentosO == 'SI') {  ?>
                                <div class="col-md-4" id="banerPorcentajeO">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Porcentaje</label>
                                        <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" name="porcentajeO" id="porcentajeO" value="<?php echo $porcentajeO; ?>" disabled="" selected="">
                                    </div>
                                </div>

                                <div class="col-md-4" id="banerObservacionPorcentajeO">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                        <textarea class="form-control" name="observacionO"  id="observacionO" rows="3"><?php echo $observacion_nordO; ?></textarea>
                                    </div>
                                </div>
                            <?php } else if ($cuenta_descuentosO == 'NO') { ?>
                                <div class="col-md-4" id="banerPorcentajeO" style="display: none">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Porcentaje</label>
                                        <input type="number" maxlength="2" max="99" min='0' class="form-control selectpicker" data-style="btn btn-link" id="porcentajeO" value="<?php echo $porcentajeO; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4" id="banerObservacionPorcentajeS" style="display: none">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Obsevaci&oacute;n</label>
                                        <textarea class="form-control" id="observacionO" rows="3"><?php echo $observacion_nordO; ?></textarea>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-4"></div>


                    </div>

                <?php } else { ?>
                    <div class="col-md-4" id="banerPQnoStockO">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Porque no cuenta con stock <span class="text-danger">*</span></label>
                            <select class="form-control selectpicker" data-style="btn btn-link" id="PorQNoStockO" name="PorQNoStockO" selected="" disabled="">
                                <option value=""><?php echo $no_cuenta_stockO; ?></option>
                            </select>
                        </div>
                    </div><br><br>

                    <?php if ($no_cuenta_stockO == 'NO LLEGA FORMULACION') { ?>
                        <div class="col-md-4" id="banerMotAgotadoO" style="display: none;">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoO" name="MotivoAgotadoO" disabled="" selected="">
                                    <option value=""><?php echo $motivo_agotadoO; ?></option>
                                </select>
                            </div>
                        </div>
                    <?php } else if ($no_cuenta_stockO == 'TRANSFERENCIA') { ?>
                        <div class="col-md-4" id="banerMotAgotadoO" style="display: none;">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoO" name="MotivoAgotadoO" disabled="" selected="">
                                    <option value=""><?php echo $motivo_agotadoO; ?></option>
                                </select>
                            </div>
                        </div>
                    <?php } else if ($no_cuenta_stockO == 'NO VECTORIZADO') { ?>
                        <div class="col-md-4" id="banerMotAgotadoO" style="display: none;">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoO" name="MotivoAgotadoO" disabled="" selected="">
                                    <option value=""><?php echo $motivo_agotadoO; ?></option>
                                </select>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-4" id="banerMotAgotadoO">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Motivo de agotado</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="MotivoAgotadoO" name="MotivoAgotadoO" disabled="" selected="">
                                    <option value=""><?php echo $motivo_agotadoO; ?></option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>


                    <?php if ($motivo_agotadoO == 'PEDIDO EN TRAMITE') { ?>
                        <div class="col-md-4" id="banerPedidoTramiteO">
                            <div class="form-group">
                                <label class="label-control">Fecha aproximada de llegada</label><br>
                                <input disabled="" selected="" type="date" min="<?php echo date('Y-m-d'); ?>" id="fechaApoxiLlegadaO" name="fechaApoxiLlegadaO" class="form-control" value='<?php echo $fech_prox_llegadaO; ?>' />
                            </div>
                        </div>

                    <?php } else if ($motivo_agotadoO == 'ALERTA') { ?>
                        <div class="col-md-4" id="banerAlertaO">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Alerta Observaci&oacute;n</label>
                                <textarea class="form-control" id="alerta_observacion" name="alerta_observacionO" rows="3" disabled="" selected=""><?php echo $alerta_observacionO; ?></textarea>
                            </div>
                        </div><br>
                    <?php } ?>




                    <div class="col-md-4"></div>


                    </div>
                <?php } ?>

            </div>
            <!--<-<-<-<--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---Compromiso------------>



            <div class="card-collapse" id="Compromiso">
                <div class="card-header" role="tab" id="headingSix">
                    <h5 class="mb-0">
                        <!-- <b>Compromiso <span class="text-danger">*</span></b> -->
                    </h5>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Texto</label>
                            <textarea class="form-control" id="compromiso" name="compromiso" rows="3" disabled="" selected=""><?php #echo #$compromisoe; 
                                                                                                                                ?></textarea>

                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-5"><button type="button" onclick=" location.href='table_route.php'" class="btn btn-danger mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Volver </button> </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>





            </div>
        </div>
    </div>

<?php } ?>

</div>
</div>
</div>


</div>
</form>
</div>
<footer class="footer">
    <div class="container-fluid">

        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
        </div>
    </div>
</footer>
</div>
</div>
<script>
    function setFormValidation(id) {
        $(id).validate({
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').append(error);
            },
        });
    }

    $(document).ready(function() {
        setFormValidation('#RegisterValidation');
        setFormValidation('#TypeValidation');
        setFormValidation('#LoginValidation');
        setFormValidation('#RangeValidation');
    });
</script>
<!--   Core JS Files   -->

<script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
<script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->

<script>
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "Tot"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Buscar registros",
            }
        });

        var table = $('#datatable').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });
    });
</script>
<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function(event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function() {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function() {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function() {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function() {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function() {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>
<script>
    $('#myModal').modal(options)
</script>

</body>

</html>